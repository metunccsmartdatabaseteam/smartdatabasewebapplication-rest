﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using SmartDatabaseWebApplication.Classes;

namespace SmartDatabaseWebApplication.Controllers
{
    public class LocationController : ApiController
    {
        // GET: api/Location
        //[RequireHttps]
        [Route("Location/")]
        public string Get()
        {
            return "You are in the wrong neighborhood";
        }

        // GET: api/Location/5
        //[RequireHttps]
        [Route("Location/{id}")]
        public string Get(int id)
        {
            return id.ToString();
        }

        /// <summary>
        /// Updates Given User's Location
        /// </summary>
        /// <param name="id"></param>
        /// <param name="latitude"></param>
        /// <param name="longitude"></param>
        /// <param name="time"></param>
        /// <returns></returns>
        [Route("Location/{id},{latitude},{longitude},{time},{heartBeat}")]
        ////[RequireHttps]
        public async System.Threading.Tasks.Task<bool> Post(string id, string latitude, string longitude, string time,string heartBeat)
        {
            return await new LocationAndDisasterControl().AddUserLocationAsync(id,latitude,longitude,time,heartBeat);
        }

        /// <summary>
        /// Updates Given User's Location and MAC Address
        /// </summary>
        /// <param name="id"></param>
        /// <param name="latitude"></param>
        /// <param name="longitude"></param>
        /// <param name="mac"></param>
        /// <param name="time"></param>
        /// <returns></returns>
        [Route("LocationAndMac/{id},{latitude},{longitude},{mac},{time},{heartBeat}")]
        //[RequireHttps]
        public async System.Threading.Tasks.Task<bool> Post(string id, string latitude, string longitude,string mac ,string time,string heartBeat)
        {
            return await new LocationAndDisasterControl().AddUserLocationAndMacAsync(id, latitude, longitude, mac , time, heartBeat);
        }

        /// <summary>
        /// Updates Given User's MAC Address
        /// </summary>
        /// <param name="id"></param>
        /// <param name="mac"></param>
        /// <returns></returns>
        [Route("MAC/{id},{mac}")]
        //[RequireHttps]
        public async System.Threading.Tasks.Task<bool> Post(string id, string mac)
        {
            return await new LocationAndDisasterControl().AddUserMacAsync(id,mac);
        }


        /// <summary>
        /// Updates Given User's Nearbys According to Given MAC List
        /// </summary>
        /// <param name="id"></param>
        /// <param name="nearbyMACs"></param>
        /// <param name="time"></param>
        /// <returns></returns>
        [Route("Nearby/{id},{nearbyMACs},{time},{heartBeat},{latitude},{longitude}")]
        //[RequireHttps]
        public async System.Threading.Tasks.Task<bool> Post(string id, string nearbyMACs, string time,string heartBeat,float latitude,float longitude)
        {
            return await new LocationAndDisasterControl().SetNearbysAsync(id, nearbyMACs,time,heartBeat,latitude,longitude );
        }

        /// <summary>
        /// Create an Disaster Instance for Admins
        /// </summary>
        /// <param name="latitude"></param>
        /// <param name="longitude"></param>
        /// <param name="radius"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        [Route("Disaster/{latitude},{longitude},{radius},{description}")]
        //[RequireHttps]
        public bool Post(float latitude, float longitude, float radius,string description)
        {
            return  new LocationAndDisasterControl().AddDisaster(latitude,longitude,radius,description);
        }

        /// <summary>
        /// Deletes an Disaster Instance
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("Disaster/{id}")]
        //[RequireHttps]
        public bool Post(int id)
        {
            return new LocationAndDisasterControl().DeleteDisaster(id);
        }

        // PUT: api/Location/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Location/5
        public void Delete(int id)
        {
        }
    }
}
