﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SmartDatabaseWebApplication.Classes;
using SmartDatabaseWebApplication.Classes.LogicClasses;
using System.Threading.Tasks;
using SimpleCrypto;

namespace SmartDatabaseWebApplication.Controllers
{
    public class LoginController : ApiController
    {
        // GET: api/Login
        //[RequireHttps]
        public string AdminPanelView()
        {
            return "Please Enter Credentials";
        }

        /// <summary>
        /// ID and Password Check for User Login
        /// </summary>
        /// <param name="id"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        [Route("Login/{id},{password}")]
        //[RequireHttps]
        public async Task<bool> Get(string id,string password)
        {
            
            return await new RegistrationControl().IsPasswordCorrectAsync(id,password);
        }

        /// <summary>
        /// Account Existence Check
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("Login/{id}")]
        //[RequireHttps]
        public async Task<bool> Get(string id)
        {
            return await new RegistrationControl().IsRegisteredAsync(id);
        }


        /// <summary>
        /// Full User Insertion, Including Health Credentials
        /// </summary>
        /// <param name="name"></param>
        /// <param name="mail"></param>
        /// <param name="password"></param>
        /// <param name="bloodType"></param>
        /// <param name="allergies"></param>
        /// <param name="chronics"></param>
        /// <param name="photo"></param>
        [Route("Signup/{name},{mail},{password},{bloodType},{allergies},{chronics},{photo}")]
        //[RequireHttps]
        public async Task<bool> Post(string name, string mail, string password, string bloodType, string allergies, string chronics, string photo)
        {
            var crypto = new SimpleCrypto.PBKDF2();
            var userSalt = crypto.GenerateSalt();


            return await new RegistrationControl().AddFullUser(name,mail,crypto.Compute(password),bloodType,allergies,chronics,photo,userSalt);
        }

        // PUT: api/Login/5
        public void Put(int id, [FromBody]string value)
        {
            
        }

        // DELETE: api/Login/5
        public void Delete(int id)
        {
        }
    }
}
