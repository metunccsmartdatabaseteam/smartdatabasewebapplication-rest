﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SmartDatabaseWebApplication.Classes;
using SmartDatabaseWebApplication.Classes.LogicClasses;
using System.Data;
using System.Threading.Tasks;


namespace SmartDatabaseWebApplication.Controllers
{
    public class ProfileController : ApiController
    {
        // GET: api/Profile

        [Route("Profile/")]
        public async Task<Classes.LogicClasses.User> Get()
        {
            var temp = await new CosmosConnection().GetAllUsers();
            return temp[0];
        }

       /// <summary>
       /// Returns the given mail address owner's profile credentials 
       /// </summary>
       /// <param name="mail"></param>
       /// <returns></returns>
        [Route("Profile/{mail}")]
        //[RequireHttps]
        public async Task<SmartDatabaseWebApplication.Classes.LogicClasses.User> Get(string mail)
        {
            var user = await new CosmosConnection().GetUser(mail);
            user.Salt = "";
            user.password = "";
            return user;
        }

        /// <summary>
        /// Returns All the Chronic Entries for Given User
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("Test/{query}")]
        //[RequireHttps]
        public async Task<List<User>> Get(string query,int i =0)
        {
            return await new CosmosConnection().ExecutePanelQuery(query);
        }

        /// <summary>
        /// Returns Photo of the Given User
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("Photo/{id}")]
        //[RequireHttps]
        public string Get(string id, string i = "")
        {
            return new RegistrationControl().GetUserPhoto(id);
        }

        /// <summary>
        /// Creates an Allergy Entry for the Given User
        /// </summary>
        /// <param name="id"></param>
        /// <param name="allergieName"></param>
        [Route("Allergy/{id},{allergyName}")]
        //[RequireHttps]
        public async System.Threading.Tasks.Task<bool> Post(string id, string allergyName)
        {
            return await new RegistrationControl().AddAllergyAsync(id, allergyName);
        }

        /// <summary>
        /// Creates an Chronic Entry for the Given User
        /// </summary>
        /// <param name="id"></param>
        /// <param name="chronicName"></param>
        /// <param name="i"></param>
        [Route("Chronic/{id},{chronicName}")]
        //[RequireHttps]
        public async System.Threading.Tasks.Task<bool> Post(string id, string chronicName, int i=0)
        {
            return await new RegistrationControl().AddChronicAsync(id, chronicName);
        }


        /// <summary>
        /// Creates an Photo Entry for the Given User
        /// </summary>
        /// <param name="id"></param>
        /// <param name="photo"></param>
        /// <param name="i"></param>
        [Route("Photo/{id},{photo}")]
        //[RequireHttps]
        public async Task<bool>  Post(string id, string photo, string i = "")
        {
            return await new RegistrationControl().AddPhoto(id, photo);
        }


        /// <summary>
        /// Updates the Photo Entry for the Given User
        /// </summary>
        /// <param name="id"></param>
        /// <param name="photo"></param>
        /// <param name="i"></param>
        [Route("Photo/{id},{photo},{update}")]
        //[RequireHttps]
        public async Task<bool> Post(string id, string photo, bool update)
        {
            return await new RegistrationControl().UpdatePhoto(id, photo);
        }


        /// <summary>
        /// Updates the Blood Type Entry for the Given User
        /// </summary>
        /// <param name="id"></param>
        /// <param name="photo"></param>
        /// <param name="i"></param>
        [Route("BloodType/{id},{bloodType}")]
        //[RequireHttps]
        public async Task<bool> Post(string id, string bloodType, float i =0)
        {
            return await new RegistrationControl().UpdateBloodType(id, bloodType);
        }

        // PUT: api/Profile/5
        public void Put(int id, [FromBody]string value)
        {
        }

        /// <summary>
        /// Deletes the Allergie Entry from the Given User
        /// </summary>
        /// <param name="id"></param>
        /// <param name="allergieName"></param>
        /// <returns></returns>
        [Route("Allergy/{id},{allergyName}")]
        //[RequireHttps]
        public bool Delete(string id, string allergyName)
        {
            return new RegistrationControl().DeleteAllergy(id,allergyName);
        }

        /// <summary>
        /// Deletes the Chronic Entry from the Given User
        /// </summary>
        /// <param name="id"></param>
        /// <param name="allergieName"></param>
        /// <returns></returns>
        [Route("Chronic/{id},{chronicName}")]
        //[RequireHttps]
        public bool Delete(string id, string chronicName, int i = 0)
        {
            return new RegistrationControl().DeleteChronic(id, chronicName);
        }
    }
}
