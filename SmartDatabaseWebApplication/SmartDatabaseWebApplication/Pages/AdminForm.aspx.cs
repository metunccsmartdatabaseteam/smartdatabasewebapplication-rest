﻿using SmartDatabaseWebApplication.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SmartDatabaseWebApplication.Pages
{
    public partial class AdminForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckPageValidation();

        }

        protected void btnCreateAdmin_Click(object sender, EventArgs e)
        {

            if(CheckInputs())
            {
                string inpt = Request.QueryString["token"].ToString();
                string mail = Request.QueryString["mail"].ToString();
                char canAdd = inpt[inpt.Length - 2];
                char canRemove = inpt[inpt.Length - 1];

                if (new AdminControl().AddAdmin(inpAdminID.Text.Trim(), inpAdminPass.Text.Trim(), inpAdminName.Text.Trim(), mail,int.Parse(canRemove.ToString()),int.Parse(canAdd.ToString())))
                {
                    ShowSuccessMessage("Success !!");
                    Response.Redirect("Login.aspx");
                    return;
                }

                ShowErrorMessage("Unable to Create Admin Account !");

            }

        }

        private void CheckPageValidation()
        {

            try
            {
                string inpt = Request.QueryString["token"].ToString();
                char canAdd = inpt[inpt.Length - 2];
                char canRemove = inpt[inpt.Length - 1];
                string token = "";

                for (int i = 0; i < inpt.Length - 2; i++)
                {
                    token += inpt[i];
                }

                var check = new AdminControl().CheckAdminToken(new Utilities().Decode(token));



                if (!check)
                {
                    Response.Redirect("Login.aspx");
                }
            }
            catch(Exception ex)
            {
                Response.Redirect("Login.aspx");
            }
            

            
        }


        private bool CheckInputs()
        {
            if (inpAdminID.Text.Length == 0)
            {
                ShowErrorMessage("Please Enter ID");
                return false;
            }

            if (inpAdminName.Text.Length == 0)
            {
                ShowErrorMessage("Please Enter Name");
                return false;
            }

            if (inpAdminPass.Text.Length == 0)
            {
                ShowErrorMessage("Please Enter Password");
                return false;
            }

            if (inpAdminPassAgain.Text.Length == 0)
            {
                ShowErrorMessage("Please Enter Password Again");
                return false;
            }

            if (!inpAdminPassAgain.Text.Equals(inpAdminPass.Text))
            {
                ShowErrorMessage("Passwords Does NOT Match");
                return false;
            }

            return true;
        }

        #region Message Panel Methods

        private void ShowErrorMessage(string text)
        {
            successMessageDiv.Visible = false;
            errorMessage.Text = text;
            errorMessage.Visible = true;
            errorMessageDiv.Visible = true;
        }

        private void ShowSuccessMessage(string text)
        {
            errorMessageDiv.Visible = false;
            successMessage.Text = text;
            successMessage.Visible = true;
            successMessageDiv.Visible = true;
        }

        #endregion
    }
}