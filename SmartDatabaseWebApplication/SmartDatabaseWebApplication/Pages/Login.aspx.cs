﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SmartDatabaseWebApplication.Classes;
using System.Threading.Tasks;

namespace SmartDatabaseWebApplication.Pages
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {



        }

        #region Button Click Methods

        protected void CreateTokenClick(object sender, EventArgs e)
        {
            if(inpAdminID.Text.Length > 0)
            {
                if(inpAdminPass.Text.Length > 0)
                {

                    var res = new AdminControl().CreateAdminToken(inpAdminID.Text, inpAdminPass.Text);

                    if(res)
                    {
                        ShowSuccessMessage("Your token has been sent to your e-mail address");
                    }
                    else
                    {
                        ShowErrorMessage("Wrong Credentials! Try Again.");
                    }
                
                }
                else
                {
                    ShowErrorMessage("You cannot left password field blank.");

                }
            }
            else
            {
                ShowErrorMessage("You cannot left ID field blank.");
            }
        }


        protected void LoginClick(object sender, EventArgs e)
        {
            if(inpToken.Text.Length > 0)
            {
                var res = new AdminControl().CheckAdminToken(inpToken.Text);

                if(res)
                {
                    if(inpAdminID.Text.Length > 0)
                    {
                        var admin = new AdminControl().GetAdmin(inpAdminID.Text);

                        System.Web.HttpContext.Current.Session["E-mail"] = admin;
                        System.Web.HttpContext.Current.Session["ID"] = inpAdminID.Text;
                        System.Web.HttpContext.Current.Session["CanRemove"] = admin.CanRemove;
                        System.Web.HttpContext.Current.Session["CanAdd"] = admin.CanAdd;
                        System.Web.HttpContext.Current.Session["Name"] = admin.Name;
                        System.Web.HttpContext.Current.Session.Timeout = 15;

                        Response.Redirect("AdminPanel.aspx");

                    }
                    else
                    {
                        ShowErrorMessage("Please enter your id!");
                    }
                }
                else
                {
                    Session["E-mail"] = null;
                    Session["ID"] = null;
                    Session["CanRemove"] = null;
                    Session["CanAdd"] = null;
                    Session["Name"] = null;
                    ShowErrorMessage("Invalid Token!!");
                }

            }
            else
            {
                ShowErrorMessage("Please enter a token!");
            }
        }

        #endregion


        #region Message Panel Methods

        private void ShowErrorMessage(string text)
        {
            successMessageDiv.Visible = false;
            errorMessage.Text = text;
            errorMessage.Visible = true;
            errorMessageDiv.Visible = true;
        }

        private void ShowSuccessMessage(string text)
        {
            errorMessageDiv.Visible = false;
            successMessage.Text = text;
            successMessage.Visible = true;
            successMessageDiv.Visible = true;
        }

        #endregion
    }
}