﻿using SmartDatabaseWebApplication.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SmartDatabaseWebApplication.Pages
{
    public partial class AdminManagement : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessionValidity();

            if(!IsPostBack)
            {
                InitializeAdmins();
            }
        }

        private void CheckSessionValidity()
        {
            try
            {
                string sessionAdminID = Session["ID"].ToString();

            }
            catch (Exception ex)
            {
                Response.Redirect("Login.aspx");

            }
        }

        protected void btnSendAdminInv_Click(object sender, EventArgs e)
        {
            if(Session["CanAdd"].Equals(false))
            {
                ShowErrorMessage("You Do NOT Have Permission to Add a New Admin!!");
                return;
            }

            if(inpAdminMail.Text.Trim().Length == 0)
            {
                ShowErrorMessage("You Need to Enter a Mail Address");
                return;
            }

            try
            {
                var token = new AdminControl().CreateInvToken(Session["ID"].ToString());
                StringBuilder builder = new StringBuilder();

                builder.Append("<h1>Hello!</h1></br>");
                builder.Append("<p>You have been invited to be an admin in Smart Database System by : " + Session["Name"]+"</p></br></br>");
                builder.Append("<p>You can set your credentials by following the form in the following link : </p></br></br>");
                builder.Append("<fieldset><legend>Your Admin Form Link</legend><div>"+ "<a href='https://smartdatabasewebapplication.azurewebsites.net/Pages/AdminForm.aspx?token=" + new Utilities().Encode(token) + ddlCanAdd.SelectedItem.Value + ddlCanRemove.SelectedItem.Value + "&mail=" + inpAdminMail.Text.Trim()+"'>Here Is Your Link, Definetely NOT a Virus:)</a></div></fieldset>");
                builder.Append("</br></br></br></br>");
                builder.Append("<h3>Have Fun!</h3>");
   

                


                Utilities u = new Utilities();
                u.SendMail(inpAdminMail.Text.Trim(), builder.ToString(), "Admin Invitation for Smart Database System!");

                ShowSuccessMessage("Mail Has Been Sent!");
            }
            catch(Exception ex)
            {
                ShowErrorMessage("Unable to Send the Mail");
            }
            


        }


        protected void btnDeleteAdmin_Click(object sender, EventArgs e)
        {
            if (Session["CanRemove"].Equals(false))
            {
                ShowErrorMessage("You Do NOT Have Permission to Remove an Admin!!");
                return;
            }

            var id = ddlActiveAdmins.SelectedItem.Value;

            new AdminControl().DeleteAdmin(id);
        }


        private void InitializeAdmins()
        {
            DataTable admins = new AdminControl().GetAdminList();

            ddlActiveAdmins.Items.Clear();
            if(admins != null)
            {
                for(int i = 0; i < admins.Rows.Count; i++)
                {
                    ddlActiveAdmins.Items.Add(new ListItem(admins.Rows[i][0].ToString(), admins.Rows[i][1].ToString()));
                }
            }
        }

        #region Message Panel Methods
        private void ShowErrorMessage(string text)
        {
            successMessageDiv.Visible = false;
            errorMessage.Text = text;
            errorMessage.Visible = true;
            errorMessageDiv.Visible = true;
        }

        private void ShowSuccessMessage(string text)
        {
            errorMessageDiv.Visible = false;
            successMessage.Text = text;
            successMessage.Visible = true;
            successMessageDiv.Visible = true;
        }


        #endregion

        protected void ddlCanAdd_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(Session["CanAdd"].Equals(false) && Session["CanRemove"].Equals(false))
            {
                ddlCanAdd.SelectedIndex = 0;
                ShowErrorMessage("You Do NOT Have Enough Permissions to Give Add Permission!");
            }
        }

        protected void ddlCanRemove_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Session["CanAdd"].Equals(false) && Session["CanRemove"].Equals(false))
            {
                ddlCanRemove.SelectedIndex = 0;
                ShowErrorMessage("You Do NOT Have Enough Permissions to Give Remove Permission!");
            }
        }
    }
}