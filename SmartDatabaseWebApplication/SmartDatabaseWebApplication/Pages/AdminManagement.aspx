﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdminManagement.aspx.cs" Inherits="SmartDatabaseWebApplication.Pages.AdminManagement" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Admin Management</title>
    <link rel="stylesheet" href="~/CSS/disasterStyle.css" />

    <style>
        .sidenav {
            height: 100%;
            width: 0;
            position: fixed;
            z-index: 1;
            top: 0;
            left: 0;
            background: url(../Image/navBarAfter.png) fixed 100% white;
            overflow-x: hidden;
            transition: 0.5s;
            padding-top: 60px;
        }

            .sidenav a {
                padding: 8px 8px 8px 32px;
                text-decoration: none;
                font-size: 25px;
                color: white;
                display: block;
                transition: 0.3s;
            }

                .sidenav a:hover {
                    color: darkgrey;
                }

            .sidenav .closebtn {
                position: absolute;
                top: 0;
                right: 25px;
                font-size: 36px;
                margin-left: 50px;
            }

        #main {
            transition: margin-left .5s;
            padding: 16px;
        }
    </style>
</head>
<body>

    <%-- Nav Bar --%>
    <div id="sidenavbar" class="sidenav">
        <a href="javascript:void(0)" class="closebtn" type="button" onclick="closeNav()">&times;</a>
        <a href="AdminPanel.aspx">Main</a>
        <a href="DisasterPanel.aspx">Disasters</a>
        <a href="QueryPanel.aspx">Queries</a>
        <a href="AdminManagement.aspx">Admins</a>
    </div>
    <span style="font-size: 30px; cursor: pointer; color: white; font-family: Helvetica;" onclick="openNav()">&#9776;>></span>

    <div id="main">
        <form runat="server" id="addAdminForm" style="margin-left: 150px; margin-top: 150px;">
            <table>
                <tr>
                    <td>
                        <div id="DisasterRegisterForm" runat="server" style="width: 280px; height: 360px; margin-left: 200px auto; background: white; border-radius: 3px; box-shadow: 0 0 10px rgba(0,0,0, .4); text-align: center; padding-top: 1px;">
                            <h3 style="color: dodgerblue">Add Addmin</h3>
                            <asp:TextBox ID="inpAdminMail" runat="server" CssClass="text-field" placeholder="New Admin Mail"></asp:TextBox>
                            <asp:DropDownList ID="ddlCanAdd" runat="server" Style="font-family: Helvetica; color: #6c6c6c; font-size: 20px; margin-top: 20px; margin-left: 0px;" OnSelectedIndexChanged="ddlCanAdd_SelectedIndexChanged">
                                <asp:ListItem Value="0">Cannot Add</asp:ListItem>
                                <asp:ListItem Value="1">Can Add</asp:ListItem>
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddlCanRemove" runat="server" Style="font-family: Helvetica; color: #6c6c6c; font-size: 20px; margin-top: 20px; margin-left: 0px;" OnSelectedIndexChanged="ddlCanRemove_SelectedIndexChanged">
                                <asp:ListItem Value="0">Cannot Remove</asp:ListItem>
                                <asp:ListItem Value="1">Can Remove</asp:ListItem>
                            </asp:DropDownList>



                            <asp:Button ID="btnSendAdminInv" runat="server" Text="Add" CssClass="button" OnClick="btnSendAdminInv_Click" Style="margin-top: 100px;" />
                        </div>
                    </td>
                    <td>
                        <div style="width: 300px;"></div>
                    </td>
                    <td>
                        <div id="adminRemoveForm" style="margin-left: 500px; width: 280px; height: 360px; margin-left: 200px auto; background: white; border-radius: 3px; box-shadow: 0 0 10px rgba(0,0,0, .4); text-align: center; padding-top: 1px;" runat="server">
                            <h3 style="color: dodgerblue">RemoveAdmin</h3>
                            <asp:DropDownList ID="ddlActiveAdmins" runat="server" Style="font-family: Helvetica; color: #6c6c6c; font-size: 20px;"></asp:DropDownList>
                            <asp:Button ID="btnDeleteAdmin" runat="server" Text="Remove" CssClass="button" Style="margin-top: 200px;" OnClick="btnDeleteAdmin_Click" />

                        </div>
                    </td>

                </tr>
            </table>

        </form>
    </div>
    <div id="errorMessageDiv" visible="false" runat="server" style="margin-top: 50px; width: 100%; height: 22%; margin-left: 0px; background-position: center; background-color: crimson; height: 50px; border: dotted; border-bottom-color: dimgrey;">
        <asp:Label ID="errorMessage" runat="server" Text="" type="text" Style="text-align: center; background-color: none; font-family: Helvetica; color: white; font-weight: bold;"></asp:Label>
    </div>
    <div id="successMessageDiv" visible="false " runat="server" style="margin-top: 50px; width: 100%; height: 22%; margin-left: 0px; background-position: center; background-color: limegreen; height: 50px; border: dotted; border-bottom-color: dimgrey;">
        <asp:Label ID="successMessage" runat="server" Text="" type="text" Style="text-align: center; background-color: none; font-family: Helvetica; color: white; font-weight: bold;"></asp:Label>
    </div>


    <%-- Javascript Part --%>
    <script>


        function closeNav() {
            document.getElementById("sidenavbar").style.width = "0";
            document.getElementById("main").style.marginLeft = "0";
        }


        function openNav() {
            document.getElementById("sidenavbar").style.width = "250px";
            document.getElementById("main").style.marginLeft = "250px";
        }


        function AdminBodyLoad() {
            return;
        }

    </script>



</body>
</html>
