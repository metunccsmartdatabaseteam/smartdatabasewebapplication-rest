﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="SmartDatabaseWebApplication.Pages.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">


<head runat="server">
    <title>Admin Login</title>
    <link rel="stylesheet" href="~/CSS/style.css" />
    <link runat="server" rel="shortcut icon" href="/Image/icon.png" type="image/x-icon"/>
    <link runat="server" rel="icon" href="/Image/icon.png" type="image/ico"/>
</head>



<body>
    <form id="form1" runat="server">
        <h2>Log In</h2>

        <asp:TextBox ID="inpAdminID" runat="server" CssClass="text-field" placeholder="Username"></asp:TextBox>
        <asp:TextBox ID="inpAdminPass" runat="server" CssClass="text-field" placeholder="Password" TextMode="Password"></asp:TextBox>
        <asp:TextBox ID="inpToken" runat="server" CssClass="text-field" placeholder="Token" TextMode="Password"></asp:TextBox>


        <asp:Button ID="btnLogin" runat="server" Text="Log In" CssClass="button" OnClick="LoginClick" />
        <asp:Button ID="btnCreateToken" runat="server" Text="Create Token" CssClass="button" OnClick="CreateTokenClick"/>
        
        
        <div id="errorMessageDiv" visible="false" runat="server" style =" margin-top:50px; width: 100%; height:22%; margin-left: 0px; background-position:center;background-color:crimson; height: 50px;border:dotted;border-bottom-color:dimgrey;" >
            <asp:Label ID="errorMessage" runat="server" Text="" type="text" style="text-align:left;background-color:none;font-family:Helvetica;color:white;font-weight: bold;"></asp:Label>
        </div>
        <div id="successMessageDiv" visible="false " runat="server" style =" margin-top:50px; width: 100%; height:22%; margin-left: 0px; background-position:center;background-color:limegreen;height:50px;border:dotted;border-bottom-color:dimgrey;">
            <asp:Label ID="successMessage" runat="server" Text="" type="text" style="text-align:left;background-color:none;font-family:Helvetica;color:white;font-weight: bold;"></asp:Label>
        </div>

    </form>
</body>






</html>
