﻿using SmartDatabaseWebApplication.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SmartDatabaseWebApplication.Pages
{
    public partial class DisasterPanel : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessionValidity();

            
            InitializeDisasterDropDown();
            
        }

        private void CheckSessionValidity()
        {
            try
            {
                string sessionAdminID = Session["ID"].ToString();

            }
            catch (Exception ex)
            {
                Response.Redirect("Login.aspx");

            }
        }



        public string GetDisastersFromService()
        {
            return new LocationAndDisasterControl().GetDisasters();
        }


        private void InitializeDisasterDropDown()
        {

            ddlActiveDisasters.Items.Clear();

            var disasters = new LocationAndDisasterControl().GetActiveDisastersAsTable();

            for (int i = 0; i < disasters.Rows.Count; i++)
            {
                
                 ddlActiveDisasters.Items.Add(new ListItem("ID: " + disasters.Rows[i][0].ToString() + " Explanation: " + disasters.Rows[i][4], disasters.Rows[i][0].ToString()));
                
            }
        }


        protected void btnAddDisaster_Click(object sender, EventArgs e)
        {

            float latitude = 0, longitude = 0, radius = 0;

            if(inptLatitude.Text.Length > 0)
            {
                if(inptLongitude.Text.Length > 0)
                {
                    if(inpRadius.Text.Length >  0)
                    {
                        if(inpExplanation.Text.Length > 0)
                        {
                            if(float.TryParse(inptLatitude.Text, out latitude))
                            {
                                if(float.TryParse(inptLongitude.Text,out longitude))
                                {
                                    if(float.TryParse(inpRadius.Text,out radius))
                                    {
                                        if(new LocationAndDisasterControl().AddDisaster(latitude,longitude,radius,inpExplanation.Text))
                                        {

                                            ShowSuccessMessage("Disaster is Successfully Added!");
                                            InitializeDisasterDropDown();

                                            inptLatitude.Text = "";
                                            inptLongitude.Text = "";
                                            inpRadius.Text = "";
                                            inptLatitude.Text = "";
                                            inptLongitude.Text = "";
                                        }
                                        else
                                        {
                                            ShowErrorMessage("Unable to Add the Disaster");
                                        }
                                    }
                                    else
                                    {
                                        ShowErrorMessage("Invalid Radius Entry");
                                    }
                                }
                                else
                                {
                                    ShowErrorMessage("Invalid Longitude Entry");
                                }
                            }
                            else
                            {
                                ShowErrorMessage("Invalid Latitude Entry");

                            }
                        }
                        else
                        {
                            ShowErrorMessage("Please Enter Explanation");
                        }
                    }
                    else
                    {
                        ShowErrorMessage("Please Enter Radius");
                    }
                }
                else
                {
                    ShowErrorMessage("Please Enter Longitude");
                }
            }
            else
            {
                ShowErrorMessage("Please Enter Latitude");
            }
        }

        protected void btnDeactivate_Click(object sender, EventArgs e)
        {

            if (ddlActiveDisasters.Items.Count.Equals(0))
            {
                ShowErrorMessage("There is No Disaster to Deactivate");
                return;
            }

            var id = ddlActiveDisasters.SelectedItem.Value.ToString();
      

            if(new LocationAndDisasterControl().DeleteDisaster(int.Parse(id)))
            {
                ShowSuccessMessage("Disaster Deactivated Successfully");
            }
            else
            {
                ShowErrorMessage("Unable to Deactivate the Disaster");
            }
        }

        #region Message Panel Methods

        private void ShowErrorMessage(string text)
        {
            successMessageDiv.Visible = false;
            errorMessage.Text = text;
            errorMessage.Visible = true;
            errorMessageDiv.Visible = true;
        }

        private void ShowSuccessMessage(string text)
        {
            errorMessageDiv.Visible = false;
            successMessage.Text = text;
            successMessage.Visible = true;
            successMessageDiv.Visible = true;
        }

        #endregion


    }
}