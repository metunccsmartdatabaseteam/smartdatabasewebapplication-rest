﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SmartDatabaseWebApplication.Classes;
using System.Threading.Tasks;
using SmartDatabaseWebApplication.Classes.LogicClasses;
using Newtonsoft.Json;

namespace SmartDatabaseWebApplication.Pages
{
    public partial class AdminPanel : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckSessionValidity();
            

        }





        #region Location Management
        public  string GetLocationsFromService()
        {
            Task<string> task = Task.Run(() => new LocationAndDisasterControl().GetLocations());
            return task.Result;
        }

        public string GetDisastersFromService()
        {
            return new LocationAndDisasterControl().GetDisasters();
        }

        public string GetGroupsFromService()
        {
            var task = Task.Run(() => new CosmosConnection().GetUsersWithNearbys());

            return task.Result;
        }

        public string GetLocs()
        {
            return new LocationAndDisasterControl().GetLocs();
        }
        #endregion

        #region Session Management

        private void CheckSessionValidity()
        {
            try
            {
                string sessionAdminID = Session["ID"].ToString();

            }
            catch (Exception ex)
            {
                Response.Redirect("Login.aspx");

            }
        }
        #endregion
    }
}