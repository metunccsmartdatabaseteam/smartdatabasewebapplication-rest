﻿using SmartDatabaseWebApplication.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using System.Data;


namespace SmartDatabaseWebApplication.Pages
{
    public partial class QueryPanel : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            CheckSessionValidity();

            if (CheckQuery())
            {
                ExecuteQuery(Request.QueryString["q"].ToString());
            }
        }


        public void ExecuteQuery(string query)
        {
            Task<List<Classes.LogicClasses.User>> task = Task.Run(() => new CosmosConnection().ExecutePanelQuery(query));
            var result = task.Result;

            resultTable.Rows.Clear();

            TableRow attributeNames = new TableRow();
            resultTable.Rows.Add(attributeNames);

            if(result.Count > 0)
            {
                var temp = result[0];

                var isMap = CreateColumns(attributeNames, temp);

                CreateRows(result);

                ///If the map will be used, call javascript
                if(isMap == 4)
                {
                    DataTable locations = new DataTable();

                    locations.Columns.Add(new DataColumn("name"));
                    locations.Columns.Add(new DataColumn("lat"));
                    locations.Columns.Add(new DataColumn("lng"));
                    locations.Columns.Add(new DataColumn("time"));

                    foreach(var x in result)
                    {
                        locations.Rows.Add(x.name,x.location.latitude.ToString(), x.location.longitude, x.location.time);
                    }

                    hiddenValue.Value = JsonConvert.SerializeObject(locations);
                   
                }

            }





        }

        private void CreateRows(List<Classes.LogicClasses.User> result)
        {
            foreach (var x in result)
            {
                TableRow row = new TableRow();
                resultTable.Rows.Add(row);

                if (x.allergies != null)
                {
                    TableCell cell = new TableCell();
                    cell.Text = JsonConvert.SerializeObject(x.allergies);
                    row.Cells.Add(cell);
                }

                if (x.bloodType != null)
                {
                    TableCell cell = new TableCell();
                    cell.Text = x.bloodType;
                    row.Cells.Add(cell);
                }

                if (x.chronics != null)
                {
                    TableCell cell = new TableCell();
                    cell.Text = JsonConvert.SerializeObject(x.chronics);
                    row.Cells.Add(cell);
                }

                if (x.heartBeat != null)
                {
                    TableCell cell = new TableCell();
                    cell.Text = x.heartBeat;
                    row.Cells.Add(cell);
                }


                try
                { 
                    if (!x.location.latitude.Equals(0))
                    {
                        TableCell cell = new TableCell();
                        cell.Text = x.location.latitude.ToString();
                        row.Cells.Add(cell);
                    }


                    if (!x.location.longitude.Equals(0))
                    {
                        TableCell cell = new TableCell();
                        cell.Text = x.location.longitude.ToString();
                        row.Cells.Add(cell);

                    }

                    if (!x.location.time.ToString().Equals("01-Jan-01 12:00:00 AM") )
                    {
                        TableCell cell = new TableCell();
                        cell.Text = x.location.time.ToString();
                        row.Cells.Add(cell);
                    }
                }
                catch(Exception ex)
                {

                }
               


                if (x.mail != null)
                {
                    TableCell cell = new TableCell();
                    cell.Text = x.mail;
                    row.Cells.Add(cell);
                }

                if (x.name != null)
                {
                    TableCell cell = new TableCell();
                    cell.Text = x.name;
                    row.Cells.Add(cell);
                }

                if (x.nearbys != null)
                {
                    TableCell cell = new TableCell();
                    cell.Text = JsonConvert.SerializeObject(x.nearbys);
                    row.Cells.Add(cell);
                }
            }
        }

        private static int CreateColumns(TableRow attributeNames, Classes.LogicClasses.User temp)
        {

            int isMap = 0;

            if (temp.allergies != null)
            {
                TableCell cell = new TableCell();
                cell.Text = "allergies";
                attributeNames.Cells.Add(cell);
            }

            if (temp.bloodType != null)
            {
                TableCell cell = new TableCell();
                cell.Text = "bloodType";
                attributeNames.Cells.Add(cell);
            }

            if (temp.chronics != null)
            {
                TableCell cell = new TableCell();
                cell.Text = "chronics";
                attributeNames.Cells.Add(cell);
            }

            if (temp.heartBeat != null)
            {
                TableCell cell = new TableCell();
                cell.Text = "heartBeat";
                attributeNames.Cells.Add(cell);
            }

            try
            {
                if (!temp.location.latitude.Equals(0))
                {
                    TableCell cell = new TableCell();
                    cell.Text = "latitude";
                    attributeNames.Cells.Add(cell);
                    isMap++;
                }


                if (!temp.location.longitude.Equals(0))
                {
                    TableCell cell = new TableCell();
                    cell.Text = "longitude";
                    attributeNames.Cells.Add(cell);
                    isMap++;

                }

                if (!temp.location.time.ToString().Equals("01-Jan-01 12:00:00 AM"))
                {
                    TableCell cell = new TableCell();
                    cell.Text = "time";
                    attributeNames.Cells.Add(cell);
                    isMap++;
                }

                
            }
            catch(Exception ex)
            {
                return isMap;
            }

            

            if (temp.mail != null)
            {
                TableCell cell = new TableCell();
                cell.Text = "mail";
                attributeNames.Cells.Add(cell);
            }

            if (temp.name != null)
            {
                TableCell cell = new TableCell();
                cell.Text = "name";
                attributeNames.Cells.Add(cell);
                isMap++;
            }

            if (temp.nearbys != null)
            {
                TableCell cell = new TableCell();
                cell.Text = "nearbys";
                attributeNames.Cells.Add(cell);
            }

            return isMap;
        }

        private bool CheckQuery()
        {
            try
            {
                string query = Request.QueryString["q"].ToString();

                if(query != null)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        #region Session Management

        private void CheckSessionValidity()
        {
            try
            {
                string sessionAdminID = Session["ID"].ToString();

            }
            catch (Exception ex)
            {
                Response.Redirect("Login.aspx");

            }
        }
        #endregion
    }
}