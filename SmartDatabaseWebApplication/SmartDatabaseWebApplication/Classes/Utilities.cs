﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Net.Mail;
using System.Text;

namespace SmartDatabaseWebApplication.Classes
{
    public class Utilities
    {
        #region Message Control

        public bool AddMessage(string mail, string message)
        {

            try
            {
                DatabaseConnection db = new DatabaseConnection();

                SqlConnection sc;

                SqlCommand cmd = db.GetProcedureCommand("AddMessage", out sc);



                cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = mail;
                cmd.Parameters.Add("@message", SqlDbType.VarChar).Value = message;

                cmd.CommandType = CommandType.StoredProcedure;

                sc.Open();
                cmd.ExecuteNonQuery();
                sc.Close();
                return true;
            }
            catch
            {

                return false;
            }
        }

        #endregion

        #region Mail
        public void SendMail(string adminMail, string content, string subject)
        {
            MailMessage msg = new MailMessage();
            System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient();
            try
            {
                msg.Subject = subject;
                msg.Body = content;
                msg.From = new MailAddress("smartdatabaseMETU@gmail.com", "Smart Database Service");
                msg.To.Add(adminMail);
                msg.IsBodyHtml = true;
                client.Host = "smtp."+adminMail.Split('@')[1];
                System.Net.NetworkCredential basicauthenticationinfo = new System.Net.NetworkCredential("smartdatabaseMETU", "METUSmartDatabase-*0");
                client.Port = int.Parse("587");
                client.EnableSsl = true;
                client.UseDefaultCredentials = false;
                client.Credentials = basicauthenticationinfo;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.Send(msg);
            }
            catch (Exception ex)
            {

            }

        }
        #endregion
        #region Time
        public DateTime ConvertStringToTime(string time)
        {
            

            var timeSplitted = time.Split(';');

            DateTime t = new DateTime(int.Parse(timeSplitted[2]),
                int.Parse(timeSplitted[1]),
                int.Parse(timeSplitted[0]), 
                int.Parse(timeSplitted[3]),
                int.Parse(timeSplitted[4]),
                int.Parse(timeSplitted[5]));

            return t;
        }
        #endregion
        #region Encoding

        public string Encode(string str)
        {
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(str));
        }

        public string Decode(string str)
        {
            return Encoding.UTF8.GetString(Convert.FromBase64String(str));
        }
        #endregion
    }
}