﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Threading.Tasks;

namespace SmartDatabaseWebApplication.Classes
{
    public class LocationAndDisasterControl
    {
        
        #region Location and MAC
        public async System.Threading.Tasks.Task<bool> AddUserLocationAsync(string mail, string xLoc, string yLoc, string t,string heartBeat)
        {
            

            CosmosConnection connection = new CosmosConnection();

            var user = await connection.GetUser(mail);

            user.location.latitude = double.Parse(xLoc);
            user.location.longitude =double.Parse(yLoc);

            user.location.time = new Utilities().ConvertStringToTime(t);
            user.heartBeat = heartBeat;

            return await connection.UpdateUser(user);


        }

        public async System.Threading.Tasks.Task<bool> AddUserLocationAndMacAsync(string mail, string xLoc, string yLoc, string mac, string t, string heartBeat)
        {


            CosmosConnection connection = new CosmosConnection();

            var user = await connection.GetUser(mail);

            user.location.latitude = double.Parse(xLoc);
            user.location.longitude = double.Parse(yLoc);
            user.location.time = new Utilities().ConvertStringToTime(t);
            user.MAC = mac;
            user.heartBeat = heartBeat;

            return await connection.UpdateUser(user);

        }

        public bool AddDisaster(float latitude, float longitude, float radius,string description)
        {
            try
            {
                DatabaseConnection db = new DatabaseConnection();

                SqlConnection sc;

                SqlCommand cmd = db.GetProcedureCommand("AddDisaster", out sc);



                cmd.Parameters.Add("@latitude", SqlDbType.Int).Value = latitude;
                cmd.Parameters.Add("@longitude", SqlDbType.Int).Value = longitude;
                cmd.Parameters.Add("@radius", SqlDbType.Int).Value = radius;
                cmd.Parameters.Add("@descrp", SqlDbType.VarChar).Value = description;

                sc.Open();
                cmd.ExecuteNonQuery();


                sc.Close();

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool DeleteDisaster(int id)
        {
            try
            {
                DatabaseConnection db = new DatabaseConnection();

                SqlConnection sc;

                SqlCommand cmd = db.GetProcedureCommand("DeleteDisaster", out sc);



                cmd.Parameters.Add("@id", SqlDbType.Int).Value = id;
                

                sc.Open();
                cmd.ExecuteNonQuery();


                sc.Close();

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public async System.Threading.Tasks.Task<bool> AddUserMacAsync(string mail, string mac)
        {

           

            CosmosConnection connection = new CosmosConnection();

            var user = await connection.GetUser(mail);

            user.MAC = mac;

            return await connection.UpdateUser(user);


        }

        public async Task<string> GetLocations()
        {
            

            var users = await new CosmosConnection().GetAllUsers();
            DataTable table = new DataTable();

            table.Columns.Add(new DataColumn("lat"));
            table.Columns.Add(new DataColumn("lng"));
            table.Columns.Add(new DataColumn("name"));
            table.Columns.Add(new DataColumn("time"));

            if (users != null)
            {
                foreach(var x in users)
                {
                    if(x != null)
                    {
                        DataRow newRow = table.NewRow();
                        newRow["lat"] = x.location.latitude;
                        newRow["lng"] = x.location.longitude;
                        newRow["name"] = x.name;
                        newRow["time"] = x.location.time;

                        table.Rows.Add(newRow);
                    }
         
                }
            }

            return JsonConvert.SerializeObject(table, new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd HH:mm:ss" });

        }

        public string GetLocs()
        {
            DatabaseConnection db = new DatabaseConnection();

            SqlConnection sc;


            SqlCommand cmd = db.GetProcedureText("select lat=xLocation,lng=yLocation,name=userName,time = timeS from usertable", out sc);



            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            DataTable table = new DataTable();

            sc.Open();

            adapter.Fill(table);

            sc.Close();

            return JsonConvert.SerializeObject(table, new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd HH:mm:ss" });
        }

        public string GetDisasters()
        {
            DatabaseConnection db = new DatabaseConnection();

            SqlConnection sc;


            SqlCommand cmd = db.GetProcedureCommand("GetDisasters", out sc);



            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            DataTable table = new DataTable();

            sc.Open();

            adapter.Fill(table);

            sc.Close();

            return JsonConvert.SerializeObject(table, new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd HH:mm:ss" });
        }

        public DataTable GetActiveDisastersAsTable()
        {
            DatabaseConnection db = new DatabaseConnection();

            SqlConnection sc;


            SqlCommand cmd = db.GetProcedureCommand("GetActiveDisasters", out sc);



            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            DataTable table = new DataTable();

            sc.Open();

            adapter.Fill(table);

            sc.Close();

            return table;
        }

        public DataTable GetDisastersAsTable()
        {
            DatabaseConnection db = new DatabaseConnection();

            SqlConnection sc;


            SqlCommand cmd = db.GetProcedureCommand("GetDisasters", out sc);



            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            DataTable table = new DataTable();

            sc.Open();

            adapter.Fill(table);

            sc.Close();

            return table;
        }
        #endregion

        #region Nearby
        public async System.Threading.Tasks.Task<bool> AddNearbyAsync(string mail, string nearbyMAC, string time)
        {

            

            CosmosConnection connection = new CosmosConnection();

            var user = await connection.GetUser(mail);

            user.nearbys = nearbyMAC.Split(';');
            user.nearbyTime = new Utilities().ConvertStringToTime(time);
            

            return await connection.UpdateUser(user);

        }

        public async System.Threading.Tasks.Task<bool> SetNearbysAsync(string mail, string nearbyMACs, string  time, string heartBeat,float latitude,float longitude)
        {

           

            try
            {
                CosmosConnection connection = new CosmosConnection();

                var user = await connection.GetUser(mail);

                if (nearbyMACs.Equals("clear"))
                {
                    user.nearbys = null;
                }

                else
                {
                    user.nearbys = nearbyMACs.Split('|');

                    foreach (var x in user.nearbys)
                    {
                        var arr = x.Split(';');

                        var nearbyUser = await new CosmosConnection().GetUserByMAC(arr[0]);

                        nearbyUser.location.latitude = double.Parse(arr[1]);
                        nearbyUser.location.longitude = double.Parse(arr[2]);
                        nearbyUser.location.time = new Utilities().ConvertStringToTime(time);

                        await new CosmosConnection().UpdateUser(nearbyUser);

                    }
                }

                user.nearbyTime = new Utilities().ConvertStringToTime(time);

                user.heartBeat = heartBeat;
                user.location.latitude = latitude;
                user.location.longitude = longitude;
                user.location.time = new Utilities().ConvertStringToTime(time);

                return await connection.UpdateUser(user);
            }
            catch(Exception ex)
            {
                return false;
            }

            
        }

        public bool ClearNearbys(string mail)
        {

            try
            {
                DatabaseConnection db = new DatabaseConnection();

                SqlConnection sc;

                SqlCommand cmd = db.GetProcedureCommand("ClearUserNearbys", out sc);



                cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = mail;


                sc.Open();
                cmd.ExecuteNonQuery();


                sc.Close();

                return true;
            }
            catch (Exception e)
            {
                return false;
            }

        }

        public string GetUserNearbys(string mail)
        {

            try
            {
                DatabaseConnection db = new DatabaseConnection();

                SqlConnection sc;

                SqlCommand cmd = db.GetProcedureCommand("GetUserNearbys", out sc);



                cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = mail;



                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                DataTable table = new DataTable();

                sc.Open();
                adapter.Fill(table);
                sc.Close();

                return JsonConvert.SerializeObject(table);
            }
            catch (Exception e)
            {
                return "DB Error";
            }

        }
        #endregion
    }
}