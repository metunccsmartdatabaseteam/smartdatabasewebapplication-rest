﻿using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartDatabaseWebApplication.Classes
{
    public class CosmosConnection
    {
        private const string EndPoint = "https://smart-db-nosql.documents.azure.com:443/";
        private const string PrimaryKey = "Hqp4sK7lW39zBq8oe6n4X6MVyysRGV4Uy0BQptYpnCkVgYrqzwjiLPIz3teKBFche8YtklizzEsAB7PKQjN2aA==";
        private const string DatabaseName = "smart-db-nosql";
        private const string CollectionName = "AreYouSafe";

        /// <summary>
        /// Establishes connection with the NoSQL Database (Cosmos) and returns a Document Client
        /// </summary>
        /// <returns></returns>
        private async Task<DocumentClient> CreateDocumentClient()
        {

            DocumentClient client = null;
            try
            {
                client = new DocumentClient(new Uri(EndPoint), PrimaryKey);

                await client.CreateDatabaseIfNotExistsAsync(new Database { Id = DatabaseName });

                await client.CreateDocumentCollectionIfNotExistsAsync(UriFactory.CreateDatabaseUri(DatabaseName), new DocumentCollection { Id = CollectionName });

            }
            catch (Exception e)
            {
                return client;
            }

            return client;
        }


        /// <summary>
        /// Creates a document for the given user object
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<bool> CreateUser(SmartDatabaseWebApplication.Classes.LogicClasses.User user)
        {

            DocumentClient client = await CreateDocumentClient();

            try
            {
                await client.CreateDocumentAsync(CreateConnectionString(), user);
                return true;
            }
            catch (DocumentClientException readResult)
            {
                return false;
            }

            
        }
        
        /// <summary>
        /// Replaces given user's information to be the given values.
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<bool> UpdateUser(SmartDatabaseWebApplication.Classes.LogicClasses.User user)
        {

            DocumentClient client = await CreateDocumentClient();

            try
            {
                await client.ReplaceDocumentAsync(CreateConnectionString(user.mail), user);

                return true;
            }
            catch (DocumentClientException readResult)
            {
                return false;
            }


        }

        /// <summary>
        /// Creates Collection Connection Link
        /// </summary>
        /// <returns></returns>
        private static Uri CreateConnectionString()
        {
            return UriFactory.CreateDocumentCollectionUri(DatabaseName, CollectionName);
        }

        /// <summary>
        /// Creates Document Connection Link for Specific User
        /// </summary>
        /// <param name="userMail"></param>
        /// <returns></returns>
        private static Uri CreateConnectionString(string userMail)
        {
            return UriFactory.CreateDocumentUri(DatabaseName, CollectionName,userMail);
        }

        /// <summary>
        /// Returns the requested user as an object with mail identifier.
        /// </summary>
        /// <param name="mail"></param>
        /// <returns></returns>
        public async Task<SmartDatabaseWebApplication.Classes.LogicClasses.User> GetUser(string mail)
        {
            DocumentClient client = await CreateDocumentClient();

            SmartDatabaseWebApplication.Classes.LogicClasses.User user = new LogicClasses.User();

            try
            {
                
                FeedOptions queryOptions = new FeedOptions { MaxItemCount = -1 };

                
                IQueryable<SmartDatabaseWebApplication.Classes.LogicClasses.User> userQuery = client.CreateDocumentQuery<SmartDatabaseWebApplication.Classes.LogicClasses.User>(
                        CreateConnectionString()
                        ,queryOptions)
                        .Where(x => x.mail == mail);

                foreach(var x in userQuery)
                {
                    user.CopyUser(x);
                }

                return user;
            }
            catch(DocumentClientException ex)
            {
                return null;
            }

        }

        /// <summary>
        /// Given the MAC address of the user, it returns the user.
        /// </summary>
        /// <param name="MAC"></param>
        /// <returns></returns>
        public async Task<SmartDatabaseWebApplication.Classes.LogicClasses.User> GetUserByMAC(string MAC)
        {
            DocumentClient client = await CreateDocumentClient();

            SmartDatabaseWebApplication.Classes.LogicClasses.User user = new LogicClasses.User();

            try
            {

                FeedOptions queryOptions = new FeedOptions { MaxItemCount = -1 };


                IQueryable<SmartDatabaseWebApplication.Classes.LogicClasses.User> userQuery = client.CreateDocumentQuery<SmartDatabaseWebApplication.Classes.LogicClasses.User>(
                        CreateConnectionString()
                        , queryOptions)
                        .Where(x => x.MAC == MAC);

                foreach (var x in userQuery)
                {
                    user.CopyUser(x);
                }

                return user;
            }
            catch (DocumentClientException ex)
            {
                return null;
            }

        }

        /// <summary>
        /// Returns all users
        /// </summary>
        /// <param name="mail"></param>
        /// <returns></returns>
        public async Task<List<LogicClasses.User>> GetAllUsers()
        {
            DocumentClient client = await CreateDocumentClient();

            List<LogicClasses.User> users = new List<LogicClasses.User>();
            
            try
            {

                FeedOptions queryOptions = new FeedOptions { MaxItemCount = -1 };


                IQueryable<LogicClasses.User> userQuery = client.CreateDocumentQuery<LogicClasses.User>(
                        CreateConnectionString()
                        , queryOptions)
                        .Where(x => x.mail != null);

                foreach (var x in userQuery)
                {
                    LogicClasses.User temp = new LogicClasses.User();
                    temp.CopyUser(x);


                    users.Add(temp);
                }

                return users;
            }
            catch (DocumentClientException ex)
            {
                return null;
            }

        }
        /// <summary>
        /// Returns the group lists for everyone
        /// </summary>
        /// <returns></returns>
        public async Task<string> GetUsersWithNearbys()
        {
            DocumentClient client = await CreateDocumentClient();

            List<LogicClasses.User> users = new List<LogicClasses.User>();

            try
            {

                FeedOptions queryOptions = new FeedOptions { MaxItemCount = -1 };


                IQueryable<LogicClasses.User> userQuery = client.CreateDocumentQuery<LogicClasses.User>(
                        CreateConnectionString()
                        , queryOptions)
                        .Where(x => x.nearbys != null);

                foreach (var x in userQuery)
                {
                    
                    if(x.nearbys.Length > 0)
                    {
                        LogicClasses.User temp = new LogicClasses.User();
                        temp.CopyUser(x);


                        users.Add(temp);
                    }
                    
                }

                string locations = "";
               

                for(int i = 0; i < users.Count;i++)
                {

                    //Add Users Own Location
                    locations += users[i].location.latitude + ";" + users[i].location.longitude;
                    if (users[i].nearbys.Length > 0) locations += "|";

                    //Add Nearbys of the User
                    for(int j = 0;j<users[i].nearbys.Length;j++)
                    {
                        var nearby = users[i].nearbys[j].Split(';');

                        locations += nearby[1] + ";" + nearby[2];

                        if (j != users[i].nearbys.Length - 1) locations += "|";
                    }

                    if(i != users.Count-1) locations += "#";
                }


                return locations;
            }
            catch (Exception ex)
            {
                return "";
            }

        }

        /// <summary>
        /// Executes the query that is coming from the query panel
        /// </summary>
        /// <returns></returns>
        public async Task<List<LogicClasses.User>> ExecutePanelQuery(string query)
        {
            DocumentClient client = await CreateDocumentClient();

            List<LogicClasses.User> users = new List<LogicClasses.User>();

            try
            {

                FeedOptions queryOptions = new FeedOptions { MaxItemCount = -1 };


                IQueryable<LogicClasses.User> userQuery = client.CreateDocumentQuery<LogicClasses.User>(CreateConnectionString(),query,queryOptions);

                foreach (var x in userQuery)
                {
                   
                    LogicClasses.User temp = new LogicClasses.User();
                    temp.CopyUser(x);
                    users.Add(temp);

                }

                return users;
            }
            catch (DocumentClientException ex)
            {
                return null;
            }

        }
    }
}