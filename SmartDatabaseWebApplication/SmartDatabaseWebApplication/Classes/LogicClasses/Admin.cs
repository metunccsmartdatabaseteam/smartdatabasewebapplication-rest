﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartDatabaseWebApplication.Classes.LogicClasses
{
    public class Admin
    {
        private string name;
        private string mail;
        private bool canRemove;
        private bool canAdd;

        public Admin(string name, string mail, bool canRemove, bool canAdd)
        {
            this.Name = name;
            this.Mail = mail;
            this.CanRemove = canRemove;
            this.CanAdd = canAdd;
        }

        public string Name { get => name; set => name = value; }
        public string Mail { get => mail; set => mail = value; }
        public bool CanRemove { get => canRemove; set => canRemove = value; }
        public bool CanAdd { get => canAdd; set => canAdd = value; }
    }
}