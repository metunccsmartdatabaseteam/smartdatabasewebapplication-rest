﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SmartDatabaseWebApplication.Classes.LogicClasses
{
    
    public class User
    {

        [JsonProperty(PropertyName = "id")]
        public string mail { set; get; }
        public string name { set; get; }
        public string[] allergies { set; get; }
        public string[] chronics { set; get; }
        public string bloodType { set; get; }
        public Location location { set; get; }
        public string password { set; get; }
        public string[] nearbys { set; get; }
        public string photo { set; get; }
        public string MAC { set; get; }
        public string Salt { set; get; }
        public DateTime nearbyTime { set; get; }
        public string heartBeat { set; get; }

        public User(string mail, string name, string[] allergies, string[] chronics, string bloodType, Location location, string password, string[] nearbys,string photo,string MAC,string Salt,string heartBeat)
        {
            this.mail = mail;
            this.name = name;
            this.allergies = allergies;
            this.chronics = chronics;
            this.bloodType = bloodType;
            this.location = location;
            this.password = password;
            this.nearbys = nearbys;
            this.photo = photo;
            this.MAC = MAC;
            this.nearbyTime = System.DateTime.Now;
            this.Salt = Salt;
            this.heartBeat = heartBeat;
        }

        public User()
        {

            this.mail = null;
            this.name = null;
            this.allergies = null;
            this.chronics = null;
            this.bloodType = null;
            this.location = new Location();
            this.password = null;
            this.nearbys = null;
            this.photo = null;
            this.MAC = null;
            this.nearbyTime = System.DateTime.MaxValue;
            this.Salt = null;
            this.heartBeat = null;
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }

        public void CopyUser(User newUser)
        {
            this.mail = newUser.mail;
            this.name = newUser.name;
            this.allergies = newUser.allergies;
            this.chronics = newUser.chronics;
            this.bloodType = newUser.bloodType;
            this.location = newUser.location;
            this.password = newUser.password;
            this.nearbys = newUser.nearbys;
            this.photo = newUser.photo;
            this.MAC = newUser.MAC;
            this.nearbyTime = newUser.nearbyTime;
            this.Salt = newUser.Salt;
            this.heartBeat = newUser.heartBeat;

        }
    }
}