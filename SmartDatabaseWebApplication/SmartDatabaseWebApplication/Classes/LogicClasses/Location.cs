﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace SmartDatabaseWebApplication.Classes.LogicClasses
{
    public class Location
    {
       

        
        public double latitude { get; set; }
        public double longitude { get; set; }
        [JsonProperty(PropertyName = "t")]
        public DateTime time { get; set; }

        public Location(double latitude, double longitude, DateTime time)
        {
            this.latitude = latitude;
            this.longitude = longitude;
            this.time = time;
        }

        public Location()
        {
        
        }


        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }

    }
}