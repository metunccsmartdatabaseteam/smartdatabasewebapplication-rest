﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace SmartDatabaseWebApplication.Classes
{
    public class DatabaseConnection
    {
        public DatabaseConnection() { }
        public SqlConnection GetConnection()
        {


            SqlConnection sc = new SqlConnection(@"Server=tcp:smart-database-sqlserver.database.windows.net,1433;Initial Catalog=Smart_Database;Persist Security Info=False;User ID=NCCSmartDB;Password=METUSmartDatabase-*0;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");

            return sc;
        }

        public SqlCommand GetProcedureCommand(string procedureName, out SqlConnection sc)
        {

            sc = GetConnection();
            SqlCommand cmd = new SqlCommand(procedureName, sc);
            cmd.CommandType = CommandType.StoredProcedure;


            return cmd;
        }

        public SqlCommand GetProcedureText(string command, out SqlConnection sc)
        {

            sc = GetConnection();
            SqlCommand cmd = new SqlCommand(command, sc);
            cmd.CommandType = CommandType.Text;

            return cmd;
        }
    }
}