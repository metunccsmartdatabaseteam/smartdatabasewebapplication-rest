﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Threading.Tasks;

namespace SmartDatabaseWebApplication.Classes
{
    public class RegistrationControl
    {
        #region System Variables
        private const int TokenTime = 3;
        #endregion

        public RegistrationControl() { }

        #region User Creation Methods
        public async Task<bool> AddFullUser(string name, string mail, string password, string bloodType, string allergies, string chronics, string photo,string salt)
        {
            //AddFullUserSQL(name, mail, password, bloodType, allergies, chronics, photo);

            return await new CosmosConnection().CreateUser(new LogicClasses.User(mail, name, allergies.Split(';'), chronics.Split(';'), bloodType, new LogicClasses.Location(0, 0, System.DateTime.Now), password, "".Split(';'), photo,"",salt,""));


        }

        private async Task AddFullUserSQL(string name, string mail, string password, string bloodType, string allergies, string chronics, string photo)
        {
            try
            {
                DatabaseConnection db = new DatabaseConnection();
                SqlConnection sc;

                SqlCommand cmd = db.GetProcedureCommand("AddUser", out sc);
                cmd.Parameters.Add("@name", SqlDbType.VarChar).Value = name;
                cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = mail;
                cmd.Parameters.Add("@pass", SqlDbType.VarChar).Value = password;
                cmd.Parameters.Add("@bloodType", SqlDbType.VarChar).Value = bloodType;


                sc.Open();
                cmd.ExecuteNonQuery();


                sc.Close();

                var allergieList = allergies.Split(';');
                var chronicList = chronics.Split(';');


                for (int i = 0; i < allergieList.Length; i++)
                {
                    AddAllergyAsync(mail, allergieList[i]);
                }

                for (int i = 0; i < chronicList.Length; i++)
                {
                    AddChronicAsync(mail, chronicList[i]);

                }

                AddPhoto(mail, photo);

            }
            catch (Exception e)
            {

            }
        }

        //public async Task<bool> AddUser(string name, string mail, string password, string bloodType)
        //{

        //    //try
        //    //{
        //    //    DatabaseConnection db = new DatabaseConnection();
        //    //    SqlConnection sc;

        //    //    SqlCommand cmd = db.GetProcedureCommand("AddUser", out sc);



        //    //    cmd.Parameters.Add("@name", SqlDbType.VarChar).Value = name;
        //    //    cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = mail;
        //    //    cmd.Parameters.Add("@pass", SqlDbType.VarChar).Value = password;
        //    //    cmd.Parameters.Add("@bloodType", SqlDbType.VarChar).Value = bloodType;


        //    //    sc.Open();
        //    //    cmd.ExecuteNonQuery();


        //    //    sc.Close();

        //    //    return true;
        //    //}
        //    //catch (Exception e)
        //    //{
        //    //    return false;
        //    //}

        //    string temp = "", chronics = "", allergies = "";

        //    return await new CosmosConnection().CreateUser(new LogicClasses.User(mail, name, allergies.Split(';'), chronics.Split(';'), bloodType, new LogicClasses.Location(0, 0, System.DateTime.Now), password, temp.Split(';')));


        //}
        #endregion

        #region Blood Type Methods
        public async Task<bool> UpdateBloodType(string mail, string bloodType)
        {

            /*try
            {
                DatabaseConnection db = new DatabaseConnection();

                SqlConnection sc;

                SqlCommand cmd = db.GetProcedureCommand("UpdateBloodType", out sc);


                cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = mail;
                cmd.Parameters.Add("@bloodType", SqlDbType.VarChar).Value = bloodType;


                sc.Open();
                cmd.ExecuteNonQuery();


                sc.Close();

                return true;
            }
            catch (Exception e)
            {
                return false;
            }*/

            CosmosConnection connection = new CosmosConnection();

            var oldUser = await connection.GetUser(mail);

            oldUser.bloodType = bloodType;

            return await connection.UpdateUser(oldUser);

        }
        #endregion

        #region Photo 
        public async Task<bool> AddPhoto(string mail, string photo)
        {

            //try
            //{
            //    DatabaseConnection db = new DatabaseConnection();

            //    SqlConnection sc;

            //    SqlCommand cmd = db.GetProcedureCommand("AddPhoto", out sc);

            //    var pht = System.Text.Encoding.Unicode.GetBytes(photo);

            //    cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = mail;
            //    cmd.Parameters.Add("@photo", SqlDbType.Image).Value = pht;


            //    sc.Open();
            //    cmd.ExecuteNonQuery();


            //    sc.Close();

            //    return true;
            //}
            //catch (Exception e)
            //{
            //    return false;
            //}

            CosmosConnection connection = new CosmosConnection();

            var oldUser = await connection.GetUser(mail);

            oldUser.photo = photo;

            return await connection.UpdateUser(oldUser);

        }

        public async Task<bool> UpdatePhoto(string mail, string photo)
        {

            //try
            //{
            //    DatabaseConnection db = new DatabaseConnection();

            //    SqlConnection sc;

            //    SqlCommand cmd = db.GetProcedureCommand("UpdatePhoto", out sc);

            //    var pht = System.Text.Encoding.Unicode.GetBytes(photo);

            //    cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = mail;
            //    cmd.Parameters.Add("@photo", SqlDbType.Image).Value = pht;


            //    sc.Open();
            //    cmd.ExecuteNonQuery();


            //    sc.Close();

            //    return true;
            //}
            //catch (Exception e)
            //{
            //    return false;
            //}

            CosmosConnection connection = new CosmosConnection();

            var oldUser = await connection.GetUser(mail);

            oldUser.photo = photo;

            return await connection.UpdateUser(oldUser);

        }

        public string GetUserPhoto(string mail)
        {

            try
            {
                DatabaseConnection db = new DatabaseConnection();

                SqlConnection sc;

                SqlCommand cmd = db.GetProcedureCommand("GetUserPhotos", out sc);



                cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = mail;

                string photo = "";

                sc.Open();
                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    photo = System.Text.Encoding.Unicode.GetString(((byte[])reader.GetValue(0)));
                }



                sc.Close();

                return photo;


            }
            catch (Exception e)
            {
                return "DB Error";
            }

        }
        #endregion

        #region Allergy Methods
        public async Task<bool> AddAllergyAsync(string mail, string allergies)
        {

            //try
            //{
            //    DatabaseConnection db = new DatabaseConnection();

            //    SqlConnection sc;

            //    SqlCommand cmd = db.GetProcedureCommand("AddAllergie", out sc);



            //    cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = mail;
            //    cmd.Parameters.Add("@allergie", SqlDbType.VarChar).Value = allergy;


            //    sc.Open();
            //    cmd.ExecuteNonQuery();


            //    sc.Close();

            //    return true;
            //}
            //catch (Exception e)
            //{
            //    return false;
            //}

            CosmosConnection connection = new CosmosConnection();

            var oldUser = await connection.GetUser(mail);

            oldUser.allergies = allergies.Split(';');

            return await connection.UpdateUser(oldUser);

        }

        public bool DeleteAllergy(string mail, string allergy)
        {

            try
            {
                DatabaseConnection db = new DatabaseConnection();

                SqlConnection sc;

                SqlCommand cmd = db.GetProcedureCommand("DeleteAllergie", out sc);



                cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = mail;
                cmd.Parameters.Add("@allerie", SqlDbType.VarChar).Value = allergy;


                sc.Open();
                cmd.ExecuteNonQuery();


                sc.Close();

                return true;
            }
            catch (Exception e)
            {
                return false;
            }

        }



        public DataTable GetUserAllergies(string mail)
        {

            try
            {
                DatabaseConnection db = new DatabaseConnection();

                SqlConnection sc;

                SqlCommand cmd = db.GetProcedureCommand("GetUserAllergies", out sc);



                cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = mail;



                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                DataTable table = new DataTable();

                sc.Open();
                adapter.Fill(table);
                sc.Close();

                return table;
            }
            catch (Exception e)
            {
                return null;
            }

        }
        #endregion

        #region Chronic Methods
        public async Task<bool> AddChronicAsync(string mail, string chronics)
        {

            //try
            //{
            //    DatabaseConnection db = new DatabaseConnection();

            //    SqlConnection sc;

            //    SqlCommand cmd = db.GetProcedureCommand("AddChronic", out sc);



            //    cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = mail;
            //    cmd.Parameters.Add("@chronic", SqlDbType.VarChar).Value = chronic;


            //    sc.Open();
            //    cmd.ExecuteNonQuery();


            //    sc.Close();

            //    return true;
            //}
            //catch (Exception e)
            //{
            //    return false;
            //}

            CosmosConnection connection = new CosmosConnection();

            var oldUser = await connection.GetUser(mail);

            oldUser.chronics = chronics.Split(';');

            return await connection.UpdateUser(oldUser);

        }

        public bool DeleteChronic(string mail, string chronic)
        {

            try
            {
                DatabaseConnection db = new DatabaseConnection();

                SqlConnection sc;

                SqlCommand cmd = db.GetProcedureCommand("DeleteChronic", out sc);



                cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = mail;
                cmd.Parameters.Add("@chronic", SqlDbType.VarChar).Value = chronic;


                sc.Open();
                cmd.ExecuteNonQuery();


                sc.Close();

                return true;
            }
            catch (Exception e)
            {
                return false;
            }

        }

        public DataTable GetUserChronics(string mail)
        {

            try
            {
                DatabaseConnection db = new DatabaseConnection();

                SqlConnection sc;

                SqlCommand cmd = db.GetProcedureCommand("GetUserChronics", out sc);



                cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = mail;



                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                DataTable table = new DataTable();

                sc.Open();
                adapter.Fill(table);
                sc.Close();

                return table;
            }
            catch (Exception e)
            {
                return null;
            }

        }
        #endregion

        #region Login Methods
        public async Task<bool> IsRegisteredAsync(string mail)
        {
            //DatabaseConnection db = new DatabaseConnection();

            //SqlConnection sc;


            //SqlCommand cmd = db.GetProcedureText("select * from userTable where email = '" + mail + "'", out sc);


            //SqlDataReader reader;

            //sc.Open();

            //reader = cmd.ExecuteReader();

            //if (reader.HasRows)
            //{
            //    sc.Close();
            //    return true;
            //}



            //sc.Close();
            //return false;

            CosmosConnection connection = new CosmosConnection();

            var user = await connection.GetUser(mail);

            try
            {
                return (user.mail.Equals(mail));

            }
            catch(Exception ex)
            {
                return false;
            }





        }

        public async Task<bool> IsPasswordCorrectAsync(string mail, string password)

        {
            //DatabaseConnection db = new DatabaseConnection();

            //SqlConnection sc;


            //SqlCommand cmd = db.GetProcedureText("select pass from userTable where email = '" + mail + "'", out sc);

            //SqlDataReader reader;

            //sc.Open();

            //reader = cmd.ExecuteReader();


            //if (reader.HasRows)
            //{
            //    reader.Read();
            //    if (reader.GetValue(0).ToString().Equals(password))
            //    {
            //        sc.Close();
            //        return true;
            //    }

            //}
            //sc.Close();
            //return false;

            CosmosConnection connection = new CosmosConnection();

            var user = await connection.GetUser(mail);

            var crypto = new SimpleCrypto.PBKDF2();

            if(user!=null)
            {
                try
                {
                    var coming = crypto.Compute(password, user.Salt);
                    return crypto.Compare(user.password, coming);
                }catch(Exception ex)
                {
                    return false;
                }
                
            }

            return false;

            

        }
        #endregion

        #region Token
        public async Task<string> CreateTokenAsync(string mail, string password)
        {

            string token = "";

            if (await IsPasswordCorrectAsync(mail, password))
            {
                try
                {
                    byte[] currentTime = BitConverter.GetBytes(DateTime.Now.ToBinary());
                    byte[] key = Guid.NewGuid().ToByteArray();
                    token = Convert.ToBase64String(currentTime.Concat(key).ToArray());


                    DatabaseConnection db = new DatabaseConnection();

                    SqlConnection sc;

                    SqlCommand cmd = db.GetProcedureCommand("AddToken", out sc);


                    cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = mail;
                    cmd.Parameters.Add("@tokenString", SqlDbType.VarChar).Value = token;
                    cmd.Parameters.Add("@endDate", SqlDbType.DateTime).Value = DateTime.Now.AddMonths(TokenTime);

                    sc.Open();
                    cmd.ExecuteNonQuery();
                    sc.Close();

                    return token;

                }
                catch (Exception e)
                {
                    return token;
                }




            }

            return token;


        }


        public bool CheckToken(string tokenString)
        {
            DatabaseConnection db = new DatabaseConnection();

            SqlConnection sc;


            SqlCommand cmd = db.GetProcedureCommand("CheckToken", out sc);

            cmd.Parameters.Add("@tokenString", SqlDbType.VarChar).Value = tokenString;

            SqlDataReader reader;

            sc.Open();

            reader = cmd.ExecuteReader();


            if (reader.HasRows)
            {
                sc.Close();
                return true;

            }
            sc.Close();
            return false;

        }


        #endregion
    }
}