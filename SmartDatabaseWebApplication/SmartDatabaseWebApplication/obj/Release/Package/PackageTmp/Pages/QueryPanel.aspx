﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="QueryPanel.aspx.cs" Inherits="SmartDatabaseWebApplication.Pages.QueryPanel" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Query Panel</title>
    <link rel="stylesheet" href="~/CSS/disasterStyle.css" />
    <link runat="server" rel="shortcut icon" href="/Image/icon.png" type="image/x-icon"/>
    <link runat="server" rel="icon" href="/Image/icon.png" type="image/ico"/>
    <style>
        .sidenav {
            height: 100%;
            width: 0;
            position: fixed;
            z-index: 1;
            top: 0;
            left: 0;
            background: url(../Image/navBarAfter.png) fixed 100% white;
            overflow-x: hidden;
            transition: 0.5s;
            padding-top: 60px;
        }

            .sidenav a {
                padding: 8px 8px 8px 32px;
                text-decoration: none;
                font-size: 25px;
                color: white;
                display: block;
                transition: 0.3s;
            }

                .sidenav a:hover {
                    color: darkgrey;
                }

            .sidenav .closebtn {
                position: absolute;
                top: 0;
                right: 25px;
                font-size: 36px;
                margin-left: 50px;
            }

        .label {
            font-family: Helvetica;
            color: dodgerblue;
            text-align: center;
            vertical-align: middle;
        }

        .dropdown {
            font-family: Helvetica;
            color: dodgerblue;
            background-color: white;
            font-size: 20px;
        }

        .p {
            font-family: Helvetica;
            color: dodgerblue;
        }

        #main {
            transition: margin-left .5s;
            padding: 16px;
        }



        #resultTable {
            font-family: Helvetica;
            border-collapse: collapse;
            width: 100%;
            color:white;
        }

        #resultTable td, #customers th {
             border: 1px solid #ddd;
             padding: 8px;
        }

        #resultTable tr:nth-child(even) {
             background-color: dodgerblue;
        }

        #resultTable tr:nth-child(odd) {
             background-color: lightskyblue;
        }

        #resultTable tr:hover {
             background-color: #ddd;
        }

        #resultTable th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #4CAF50;
            color: white;
        }
    </style>


</head>


<body onload="OnStart()">

    <%-- Nav Bar --%>
    <div id="sidenavbar" class="sidenav">
        <a href="javascript:void(0)" class="closebtn" type="button" onclick="closeNav()">&times;</a>
        <a href="AdminPanel.aspx">Main</a>
        <a href="DisasterPanel.aspx">Disasters</a>
        <a href="QueryPanel.aspx">Queries</a>
        <a href="AdminManagement.aspx">Admins</a>
    </div>
    <span style="font-size: 30px; cursor: pointer; color: white; font-family: Helvetica;" onclick="openNav()">&#9776;>></span>

    <%-- Main Div --%>
    <div id="main">
        <h1 style="text-align: center;">Query Panel</h1>
        <div id="Map" style="width: 100%; height: 400px; border-style: solid; border-color: inherit; border-width: medium;"></div>
        <div style="margin-top: 20px;"></div>



        <table>
            <tr>
                <td>
                    <form>
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <label class="label"><b>SELECT</b></label>
                    </form>
                </td>
                <td>
                    <form id="selectForm">
                        <select id="ddlSelect" class="dropdown" style="margin-top: 50px;" multiple="multiple">
                            <option value="id">E-Mail</option>
                            <option value="name">Name</option>
                            <option value="location">Latitude</option>
                            <option value="location">Longitude</option>
                            <option value="location">Time Stamp</option>
                            <option value="chronics">Chronic Diseases</option>
                            <option value="allergies">Allergies</option>
                            <option value="nearbys">Nearbys</option>
                            <option value="heartBeat">Heart Beat</option>
                        </select>
                        <p class="p">You can select multiple with 'Ctrl' and 'Left Click'</p>
                        <button type="button" class="button" onclick="addSelectReturns()">Add Return Values</button>
                        <br />
                        <select id="ddlSelectedItems" class="dropdown" style="margin-top: 20px;" size="2" multiple="multiple">
                        </select>
                    </form>
                </td>
                <td>
                    <form>
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <label class="label"><b>FROM USERS</b></label>
                    </form>
                </td>
                <td>
                    <form>
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <label class="label"><b>WHERE</b></label>
                    </form>
                </td>
                <td>
                    <form id="whereForm">
                        <select id="ddlConditionVariables" class="dropdown" style="margin-top: 50px;">
                            <option value="id">E-Mail</option>
                            <option value="name">Name</option>
                            <option value="location.latitude">Latitude</option>
                            <option value="location.longitude">Longitude</option>
                            <option value="location.t">Time Stamp</option>
                            <option value="chronics">Chronic Diseases</option>
                            <option value="allergies">Allergies</option>
                            <option value="nearbys">Nearbys</option>
                            <option value="heartBeat">Heart Beat</option>
                        </select>
                        <select id="ddlOperator" class="dropdown" style="margin-top: 10px;">
                            <option value="=">=</option>
                            <option value="!=">!=</option>
                            <option value="<"><</option>
                            <option value=">">></option>
                        </select>
                        <div></div>
                        <p class="p" style="margin-top: 40px;">Right Operand (use single quote for strings)</p>
                        <input id="rightOperand" class="text-field" style="margin-top: 0px;">
                        <button type="button" class="button" onclick="addCondition()">Insert Condition</button>
                        <table>
                            <tr>
                                <td>
                                    <button type="button" class="button" onclick="addANDOp()" style="width: 50%; height: 50px;">Insert AND Operator</button>
                                </td>
                                <td>
                                    <button type="button" class="button" onclick="addOROp()" style="width: 50%; height: 50px;">Insert OR Operator</button>
                                </td>
                            </tr>
                        </table>


                    </form>
                </td>
                <td>
                    <form id="executionForm">
                        <p class="p" style="margin-top: 10px;">Included Conditions</p>
                        <select id="ddlConditionsAdded" class="dropdown" multiple="multiple" size="3">
                        </select>
                        <br />
                        <button type="button" class="button" onclick="executeQuery()" style="width: 50%; height: 50px;">Execute Query</button>
                        <button type="button" class="button" style="background-color: dodgerblue; font-family: Helvetica; color: white; width: 150px;" onclick="clearConditions()">Clear Conditions</button>
                        <button type="button" class="button" style="background-color: dodgerblue; font-family: Helvetica; color: white; width: 150px;" onclick="clearReturnVariables()">Clear Return Variables</button>



                    </form>
                </td>
            </tr>
        </table>


        <h1>Result Table</h1>

        <form id="tableForm" style="background: none;width:100%;height;100%"  height="100%" runat="server">
            <asp:Table ID="resultTable" runat="server">
                
            </asp:Table>

            <asp:HiddenField runat="server" ID="hiddenValue" />
        </form>

    </div>


    <%-- Javascript Part --%>
    <script type="text/javascript">


        function gMap() {



            //Map properties
            var mapSettings = {
                center: new google.maps.LatLng(35.24701, 33.02279),
                zoom: 15,
                mapTypeControl: true,
                mapTypeControlOptions: { style: google.maps.MapTypeControlStyle.DROPDOWN_MENU }
            };

            //Create Map
            var map = new google.maps.Map(document.getElementById("Map"), mapSettings);



        }


        function closeNav() {

            document.getElementById("sidenavbar").style.width = "0";
            document.getElementById("main").style.marginLeft = "0";
        }


        function openNav() {

            document.getElementById("sidenavbar").style.width = "250px";
            document.getElementById("main").style.marginLeft = "250px";
        }


        function addSelectReturns() {

            var selectList = document.getElementById("ddlSelect");
            var selectedList = document.getElementById("ddlSelectedItems");

            for (var i = 0; i < selectList.options.length; i++) {
                option = selectList.options[i];

                if (option.selected === true) {
                    var newOption = document.createElement("option");
                    newOption.text = option.text;
                    newOption.value = option.value;
                    selectedList.add(newOption);
                }
            }
        }

        function addCondition() {
            var conditionVariableList = document.getElementById("ddlConditionVariables");
            var operators = document.getElementById("ddlOperator");
            var rightOperand = document.getElementById("rightOperand");
            var addedConditions = document.getElementById("ddlConditionsAdded");

            if (rightOperand.value.length == 0) {
                alert("Please Enter Right Operand");
                return;
            }

            if (addedConditions.options.length > 0) {
                var lastCondition = addedConditions.options[addedConditions.options.length - 1].text;

                if (lastCondition != "AND" && lastCondition != "OR") {
                    alert("You cannot put a condition without OR or AND operand ");
                    return;
                }
            }


            var newOption = document.createElement("option");
            newOption.text = conditionVariableList.options[conditionVariableList.selectedIndex].text + " " + operators.options[operators.selectedIndex].text + " " + rightOperand.value;
            newOption.value = conditionVariableList.options[conditionVariableList.selectedIndex].value + " " + operators.options[operators.selectedIndex].value + " " + rightOperand.value;
            addedConditions.add(newOption);

        }

        function addANDOp() {
            var addedConditions = document.getElementById("ddlConditionsAdded");

            if (addedConditions.options.length == 0) {
                alert("There is no actual condition for AND operand to follow!");
                return;
            }

            var lastCondition = addedConditions.options[addedConditions.options.length - 1].text;

            if (lastCondition == "AND" || lastCondition == "OR") {
                alert("You cannot add AND condition after " + lastCondition);
                return;
            }

            var newOption = document.createElement("option");
            newOption.text = "AND";
            newOption.value = "AND";
            addedConditions.add(newOption);


        }

        function addOROp() {
            var addedConditions = document.getElementById("ddlConditionsAdded");

            if (addedConditions.options.length == 0) {
                alert("There is no actual condition for OR operand to follow!");
                return;
            }

            var lastCondition = addedConditions.options[addedConditions.options.length - 1].text;

            if (lastCondition == "AND" || lastCondition == "OR") {
                alert("You cannot add OR condition after " + lastCondition);
                return;
            }

            var newOption = document.createElement("option");
            newOption.text = "OR";
            newOption.value = "OR";
            addedConditions.add(newOption);
        }




        function clearConditions() {
            var addedConditions = document.getElementById("ddlConditionsAdded");

            for (var i = 0; i < addedConditions.options.length; i++) {
                addedConditions.options[i] = null;
            }
        }

        function clearReturnVariables() {
            var selectedItems = document.getElementById("ddlSelectedItems");

            for (var i = 0; i < selectedItems.options.length; i++) {
                selectedItems.options[i] = null;
            }
        }


        function executeQuery() {
            var addedConditions = document.getElementById("ddlConditionsAdded");
            var selectedItems = document.getElementById("ddlSelectedItems");

            if (selectedItems.options.length == 0) {
                alert("You need to specify the attributes for result that will return.");
                return;
            }

            var lastCondition = addedConditions.options[addedConditions.options.length - 1].text;

            if (lastCondition == "AND" || lastCondition == "OR") {
                alert("You cannot execute a query ending with " + lastCondition);
                return;
            }


            

            var query = "SELECT c." + selectedItems.options[0].value;
            var temp = selectedItems.options[0].value;
            for (var i = 1; i < selectedItems.options.length; i++)
            {
                if (selectedItems.options[i].value != temp)
                {
                    query += ",c." + selectedItems.options[i].value;
                   
                }
                temp = selectedItems.options[i].value;
                
            }

            query += " FROM c WHERE";

            for (var i = 0; i < addedConditions.options.length; i++)
            {

               

                if (addedConditions.options[i].value != "AND" && addedConditions.options[i].value != "OR") {
                    query += " c." + addedConditions.options[i].value;
                }
                else {
                    query += " " + addedConditions.options[i].value;
                }

            }
            
            window.location.replace("QueryPanel.aspx?q=" + query);

        }


        function showEveryOne(locs)
        {
            
            //Map properties
            var mapSettings = {
                center: new google.maps.LatLng(35.24701, 33.02279),
                zoom: 15,
                mapTypeControl: true,
                mapTypeControlOptions: { style: google.maps.MapTypeControlStyle.DROPDOWN_MENU }
            };

            //Location gathering
            
            var locations = JSON.parse(locs);


            //Create Map
            var map = new google.maps.Map(document.getElementById("Map"), mapSettings);


            //Marker variables and Info Windows initialization
            var infowindow = new google.maps.InfoWindow({});
            var marker, i;
            var markerList = [];

            //For each location
            for (i = 0; i < locations.length; i++) {

                //Create a marker
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i].lat, locations[i].lng),
                    map: map,
                    icon: '../Image/marker_53.png',
                });

                //and add a listener for info window of regarding marker
                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                        infowindow.setContent(locations[i].name + " at " + locations[i].time);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
                //add marker to the marker list
                markerList.push(marker);
            }

            //Clustering options, regarding zoom and grid
            var option = {
                gridSize: 150, maxZoom: 20, styles: [{
                    height: 53,
                    url: "../Image/marker_53.png",
                    width: 53
                },
                {
                    height: 56,
                    url: "../Image/marker_56.png",
                    width: 56
                },
                {
                    height: 66,
                    url: "../Image/marker_66.png",
                    width: 66
                },
                {
                    height: 78,
                    url: "../Image/marker_78.png",
                    width: 78
                },
                {
                    height: 90,
                    url: "../Image/marker_90.png",
                    width: 90
                }]
            };
            //Cluster creation
            var markerCluster = new MarkerClusterer(map, markerList, option);

        }

        function OnStart()
        {
            gMap();

            var hiddenVal = document.getElementById('hiddenValue').value;

            if (hiddenVal.length > 0)
            {
                showEveryOne(hiddenVal);
                document.getElementById('hiddenValue').value = "";
            }
        }

    </script>

    <%-- API references --%>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBR0IZ6QgP0p-gLCiremikh65ZaIfHcS3s" async defer></script>
    <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>


</body>
</html>

