﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DisasterPanel.aspx.cs" Inherits="SmartDatabaseWebApplication.Pages.DisasterPanel" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Disaster Panel</title>
    <link rel="stylesheet" href="~/CSS/disasterStyle.css" />
    <link runat="server" rel="shortcut icon" href="/Image/icon.png" type="image/x-icon"/>
    <link runat="server" rel="icon" href="/Image/icon.png" type="image/ico"/>
    <style>
        .sidenav {
            height: 100%;
            width: 0;
            position: fixed;
            z-index: 1;
            top: 0;
            left: 0;
            background: url(../Image/navBarAfter.png) fixed 100% white;
            overflow-x: hidden;
            transition: 0.5s;
            padding-top: 60px;
        }

            .sidenav a {
                padding: 8px 8px 8px 32px;
                text-decoration: none;
                font-size: 25px;
                color: white;
                display: block;
                transition: 0.3s;
            }

                .sidenav a:hover {
                    color: darkgrey;
                }

            .sidenav .closebtn {
                position: absolute;
                top: 0;
                right: 25px;
                font-size: 36px;
                margin-left: 50px;
            }

        #main {
            transition: margin-left .5s;
            padding: 16px;
        }
    </style>
</head>
<body onload="DisasterBodyLoad()">
    <%-- Nav Bar --%>
    <div id="sidenavbar" class="sidenav">
        <a href="javascript:void(0)" class="closebtn" type="button" onclick="closeNav()">&times;</a>
        <a href="AdminPanel.aspx">Main</a>
        <a href="DisasterPanel.aspx">Disasters</a>
        <a href="QueryPanel.aspx">Queries</a>
        <a href="AdminManagement.aspx">Admins</a>
    </div>
    <span style="font-size: 30px; cursor: pointer; color: white; font-family: Helvetica;" onclick="openNav()">&#9776;>></span>


    <h1 style="text-align: center;">Disaster Areas</h1>

    <%-- Main Html (Maps) --%>
    <div id="main">

        <form style="width: 100%; height: 100%; background: none;">

            <div>
                <button id="btnShowDisasterAreas" type="button" class="button" style="float: left;" onclick="showDisasters()">Refresh</button>
            </div>
            <div id="MapDisasters" style="width: 100%; height: 400px;" style="border-style: solid; border-color: inherit; border-width: medium;"></div>

        </form>
        <form style="background: none" runat="server">
            <table>
                <tr>
                    <td>
                        <div id="DisasterRegisterForm" runat="server" style="width: 280px; height: 360px; margin-left: 200px auto; background: white; border-radius: 3px; box-shadow: 0 0 10px rgba(0,0,0, .4); text-align: center; padding-top: 1px;">
                            <h3 style="color: dodgerblue">Add Disaster</h3>
                            <asp:TextBox ID="inptLatitude" runat="server" CssClass="text-field" placeholder="Latitude"></asp:TextBox>
                            <asp:TextBox ID="inptLongitude" runat="server" CssClass="text-field" placeholder="Longitude"></asp:TextBox>
                            <asp:TextBox ID="inpRadius" runat="server" CssClass="text-field" placeholder="Radius"></asp:TextBox>
                            <asp:TextBox ID="inpExplanation" runat="server" CssClass="text-field" placeholder="Explanation"></asp:TextBox>
                            <asp:Button ID="btnAddDisaster" runat="server" Text="Add" CssClass="button" OnClick="btnAddDisaster_Click" />
                        </div>
                    </td>
                    <td>
                        <div style="margin-left:10px;"></div>
                    </td>
                    <td>
                        <div id="DisasterDisableForm" style="margin-left: 500px; width: 280px; height: 360px; margin-left: 200px auto; background: white; border-radius: 3px; box-shadow: 0 0 10px rgba(0,0,0, .4); text-align: center; padding-top: 1px;" runat="server">
                            <h3 style="color: dodgerblue">Deactivate Disaster</h3>
                            <asp:DropDownList ID="ddlActiveDisasters" runat="server" Style="font-family: Helvetica; color: #6c6c6c;"></asp:DropDownList>
                            <asp:Button ID="btnDeactivate" runat="server" Text="Deactivate" CssClass="button" OnClick="btnDeactivate_Click" Style="margin-top: 200px;" />

                        </div>
                    </td>
                    <td>
                        <div style="margin-left:10px;"></div>
                    </td>
                    <td>
                        <div id="DisasterStats" onload="SetDisasterStats()" style="margin-left: 480px; width: 280px; height: 360px; margin-left: 200px auto; background: white; border-radius: 3px; box-shadow: 0 0 10px rgba(0,0,0, .4); text-align: center; padding-top: 1px;">
                            <h3 style="color: dodgerblue">Active Disaster Count</h3>
                            <label id="lblActiveDisaster" style="font-family: Helvetica"></label>
                            <h3 style="color: dodgerblue; margin-top: 75px;">Total Disaster Count</h3>
                            <label id="lblTotalDisaster" style="font-family: Helvetica"></label>
                            <h3 style="color: dodgerblue; margin-top: 75px;">Deactivated Disaster Count</h3>
                            <label id="lblDeactivatedDisaster" style="font-family: Helvetica"></label>

                        </div>

                    </td>
                </tr>
            </table>
        </form>



    </div>

    <div id="errorMessageDiv" visible="false" runat="server" style="margin-top: 50px; width: 100%; height: 22%; margin-left: 0px; background-position: center; background-color: crimson; height: 50px; border: dotted; border-bottom-color: dimgrey;">
        <asp:Label ID="errorMessage" runat="server" Text="" type="text" Style="text-align: center; background-color: none; font-family: Helvetica; color: white; font-weight: bold;"></asp:Label>
    </div>
    <div id="successMessageDiv" visible="false " runat="server" style="margin-top: 50px; width: 100%; height: 22%; margin-left: 0px; background-position: center; background-color: limegreen; height: 50px; border: dotted; border-bottom-color: dimgrey;">
        <asp:Label ID="successMessage" runat="server" Text="" type="text" Style="text-align: center; background-color: none; font-family: Helvetica; color: white; font-weight: bold;"></asp:Label>
    </div>

    <%-- End of Main Html --%>
    <%-- Javascript Part --%>
    <script>



        function showDisasters() {
            //Map properties
            var mapSettings = {
                center: new google.maps.LatLng(35.24701, 33.02279),
                zoom: 15,
                mapTypeControl: true,
                mapTypeControlOptions: { style: google.maps.MapTypeControlStyle.DROPDOWN_MENU }
            };



            //Create Disaster Map
            var map2 = new google.maps.Map(document.getElementById("MapDisasters"), mapSettings);

            //////////////////////////////////////////////////////// Circles for disasters////////////////////////////
            var tempDisasters = '<%=GetDisastersFromService()%>';
            var disasters = JSON.parse(tempDisasters);

            var j = 0;
            //For each disaster
            for (j = 0; j < disasters.length; j++) {

                var disaster = new google.maps.Circle({
                    center: new google.maps.LatLng(disasters[j].latitude, disasters[j].longitude),
                    radius: disasters[j].radius,
                    strokeColor: "#0000FF",
                    strokeOpacity: 0.8,
                    strokeWeight: 2,
                    fillColor: "#0000FF",
                    fillOpacity: 0.4
                });

                //Info window init.
                var infowindow = new google.maps.InfoWindow({
                    content: "Disaster ID: " + disasters[j].id + " at " + disasters[j].sTime + "\n Description: " + disasters[j].descrp,
                    position: new google.maps.LatLng(disasters[j].latitude, disasters[j].longitude)
                });

                disaster.addListener('click', function () {
                    infowindow.open(map2, disaster);
                });


                disaster.setMap(map2);
            }
        }


        function closeNav() {
            document.getElementById("sidenavbar").style.width = "0";
            document.getElementById("main").style.marginLeft = "0";
        }


        function openNav() {
            document.getElementById("sidenavbar").style.width = "250px";
            document.getElementById("main").style.marginLeft = "250px";
        }


        function SetDisasterStats() {

            var tempDisasters = '<%=GetDisastersFromService()%>';
            var disasters = JSON.parse(tempDisasters);
            var activeDisasters = 0;
            var totalDisaster = 0;

            totalDisaster = disasters.length;

            var j = 0;
            for (j = 0; j < disasters.length; j++) {
                if (disasters[j].isActive == 1) {
                    activeDisasters += 1;
                }
            }

            document.getElementById("lblActiveDisaster").innerHTML = activeDisasters.toString();
            document.getElementById("lblTotalDisaster").innerHTML = totalDisaster.toString();
            document.getElementById("lblDeactivatedDisaster").innerHTML = (totalDisaster - activeDisasters).toString();

        }

        function DisasterBodyLoad() {
            showDisasters();
            SetDisasterStats();
        }

    </script >

    <%-- API references --%>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBR0IZ6QgP0p-gLCiremikh65ZaIfHcS3s" async defer></script>
    <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>


    <%-- End of Javascript --%>
</body>
</html>
