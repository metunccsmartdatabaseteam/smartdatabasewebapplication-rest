﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdminForm.aspx.cs" Inherits="SmartDatabaseWebApplication.Pages.AdminForm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Admin Form</title>
    <link rel="stylesheet" href="~/CSS/style.css" />
     <link runat="server" rel="shortcut icon" href="/Image/icon.png" type="image/x-icon"/>
    <link runat="server" rel="icon" href="/Image/icon.png" type="image/ico"/>
</head>


<body>
    <form id="form1" runat="server">
        <h2>Sign Up</h2>

        <asp:TextBox ID="inpAdminID" runat="server" CssClass="text-field" placeholder="Username"></asp:TextBox>
        <asp:TextBox ID="inpAdminPass" runat="server" CssClass="text-field" placeholder="Password" TextMode="Password"></asp:TextBox>
        <asp:TextBox ID="inpAdminPassAgain" runat="server" CssClass="text-field" placeholder="Password Again" TextMode="Password"></asp:TextBox>
        <asp:TextBox ID="inpAdminName" runat="server" CssClass="text-field" placeholder="Real Name" ></asp:TextBox>


        <asp:Button ID="btnCreateAdmin" runat="server" Text="Sign Up" CssClass="button" OnClick="btnCreateAdmin_Click" />
        
        
        
        <div id="errorMessageDiv" visible="false" runat="server" style =" margin-top:50px; width: 100%; height:22%; margin-left: 0px; background-position:center;background-color:crimson; height: 50px;border:dotted;border-bottom-color:dimgrey;" >
            <asp:Label ID="errorMessage" runat="server" Text="" type="text" style="text-align:left;background-color:none;font-family:Helvetica;color:white;font-weight: bold;"></asp:Label>
        </div>
        <div id="successMessageDiv" visible="false " runat="server" style =" margin-top:50px; width: 100%; height:22%; margin-left: 0px; background-position:center;background-color:limegreen;height:50px;border:dotted;border-bottom-color:dimgrey;">
            <asp:Label ID="successMessage" runat="server" Text="" type="text" style="text-align:left;background-color:none;font-family:Helvetica;color:white;font-weight: bold;"></asp:Label>
        </div>

    </form>
</body>


</html>
