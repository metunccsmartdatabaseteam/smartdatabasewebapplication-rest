﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdminPanel.aspx.cs" Inherits="SmartDatabaseWebApplication.Pages.AdminPanel" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Admin Panel</title>
    <link rel="stylesheet" href="~/CSS/adminStyle.css" />
    <link runat="server" rel="shortcut icon" href="/Image/icon.png" type="image/x-icon"/>
    <link runat="server" rel="icon" href="/Image/icon.png" type="image/ico"/>
    <style>
        .sidenav {
            height: 100%;
            width: 0;
            position: fixed;
            z-index: 1;
            top: 0;
            left: 0;
            background: url(../Image/navBarAfter.png) fixed 100% white;
            overflow-x: hidden;
            transition: 0.5s;
            padding-top: 60px;
        }

            .sidenav a {
                padding: 8px 8px 8px 32px;
                text-decoration: none;
                font-size: 25px;
                color: white;
                display: block;
                transition: 0.3s;
            }

                .sidenav a:hover {
                    color: darkgrey;
                }

            .sidenav .closebtn {
                position: absolute;
                top: 0;
                right: 25px;
                font-size: 36px;
                margin-left: 50px;
            }

        #main {
            transition: margin-left .5s;
            padding: 16px;
        }
    </style>
</head>
<body onload="gMap()">

    <h1>Are You Safe Admin Panel</h1>

    <%-- Nav Bar --%>
    <div id="sidenavbar" class="sidenav">
        <a href="javascript:void(0)" class="closebtn" type="button" onclick="closeNav()">&times;</a>
        <a href="AdminPanel.aspx">Main</a>
        <a href="DisasterPanel.aspx">Disasters</a>
        <a href="QueryPanel.aspx">Queries</a>
        <a href="AdminManagement.aspx">Admins</a>
    </div>
    <span style="font-size: 30px; cursor: pointer; color: white; font-family: Helvetica;" onclick="openNav()">&#9776;>></span>
    <%-- End of Navbar --%>
    <%-- Main Html (Maps) --%>
    <div id="main" >

        <form style="width: 100%; height: 100%; background: none;">

            <h2 style="text-align: center;">All User Locations</h2>
            <div>
                <button id="btnShowEveryOne" type="button" class="button" style="float: left;" onclick="showEVeryOne()">Refresh</button>
            </div>
            <div id="Map" style="width: 100%; height: 400px;border-style: solid; border-color: inherit; border-width: medium;"></div>


            <h2 style="margin-top: 200px; text-align: center;">Disaster Areas</h2>
            <div>
                <button id="btnShowDisasterAreas" type="button" class="button" style="float: left;" onclick="showDisasters()">Refresh</button>
            </div>
            <div id="MapDisasters" style="width: 100%; height: 400px;border-style: solid; border-color: inherit; border-width: medium;"></div>

            <h2 style="margin-top: 200px; text-align: center;">Current Groups</h2>
            <div>
                <button id="btnShowGroups" type="button" class="button" style="float: left;" onclick="showGroups()">Refresh</button>
            </div>
            <div id="MapGroups" style="width: 100%; height: 400px; border-style: solid; border-color: inherit; border-width: medium;"></div>
        </form>
    </div>

    <%-- End of Main Html --%>


    <%-- Javascript Part --%>
    <script>

        function gMap() {

            //Map properties
            var mapSettings = {
                center: new google.maps.LatLng(35.24701, 33.02279),
                zoom: 15,
                mapTypeControl: true,
                mapTypeControlOptions: { style: google.maps.MapTypeControlStyle.DROPDOWN_MENU }
            };

            //Create Map
            var map = new google.maps.Map(document.getElementById("Map"), mapSettings);

            //Create Disaster Map
            var map2 = new google.maps.Map(document.getElementById("MapDisasters"), mapSettings);

            //Create Groups Map
            var map3 = new google.maps.Map(document.getElementById("MapGroups"), mapSettings);


        }


        function showEVeryOne() {
            //Map properties
            var mapSettings = {
                center: new google.maps.LatLng(35.24701, 33.02279),
                zoom: 15,
                mapTypeControl: true,
                mapTypeControlOptions: { style: google.maps.MapTypeControlStyle.DROPDOWN_MENU }
            };

            //Location gathering
            var temp = '<%=GetLocationsFromService()%>';
            var locations = JSON.parse(temp);


            //Create Map
            var map = new google.maps.Map(document.getElementById("Map"), mapSettings);


            //Marker variables and Info Windows initialization
            var infowindow = new google.maps.InfoWindow({});
            var marker, i;
            var markerList = [];

            //For each location
            for (i = 0; i < locations.length; i++) {

                //Create a marker
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i].lat, locations[i].lng),
                    map: map,
                    icon: '../Image/marker_53.png',
                });

                //and add a listener for info window of regarding marker
                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                        infowindow.setContent(locations[i].name + " at " + locations[i].time);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
                //add marker to the marker list
                markerList.push(marker);
            }

            //Clustering options, regarding zoom and grid
            var option = {
                gridSize: 150, maxZoom: 20, styles: [{
                    height: 53,
                    url: "../Image/marker_53.png",
                    width: 53
                },
                {
                    height: 56,
                    url: "../Image/marker_56.png",
                    width: 56
                },
                {
                    height: 66,
                    url: "../Image/marker_66.png",
                    width: 66
                },
                {
                    height: 78,
                    url: "../Image/marker_78.png",
                    width: 78
                },
                {
                    height: 90,
                    url: "../Image/marker_90.png",
                    width: 90
                }]
            };
            //Cluster creation
            var markerCluster = new MarkerClusterer(map, markerList, option);

        }

        function showDisasters() {
            //Map properties
            var mapSettings = {
                center: new google.maps.LatLng(35.24701, 33.02279),
                zoom: 15,
                mapTypeControl: true,
                mapTypeControlOptions: { style: google.maps.MapTypeControlStyle.DROPDOWN_MENU }
            };




            //Create Disaster Map
            var map2 = new google.maps.Map(document.getElementById("MapDisasters"), mapSettings);

            //////////////////////////////////////////////////////// Circles for disasters////////////////////////////
            var tempDisasters = '<%=GetDisastersFromService()%>';
            var disasters = JSON.parse(tempDisasters);

            var j = 0;
            //For each disaster
            for (j = 0; j < disasters.length; j++) {

                var disaster = new google.maps.Circle({
                    center: new google.maps.LatLng(disasters[j].latitude, disasters[j].longitude),
                    radius: disasters[j].radius,
                    strokeColor: "#0000FF",
                    strokeOpacity: 0.8,
                    strokeWeight: 2,
                    fillColor: "#0000FF",
                    fillOpacity: 0.4
                });

                //Info window init.
                var infowindow = new google.maps.InfoWindow({
                    content: "Disaster ID: " + disasters[j].id + " at " + disasters[j].sTime + "\n Description: " + disasters[j].descrp,
                    position: new google.maps.LatLng(disasters[j].latitude, disasters[j].longitude)
                });

                disaster.addListener('click', function () {
                    infowindow.open(map2, disaster);
                });


                disaster.setMap(map2);
            }
        }

        function showGroups() {
            //Map properties
            var mapSettings = {
                center: new google.maps.LatLng(35.24701, 33.02279),
                zoom: 15,
                mapTypeControl: true,
                mapTypeControlOptions: { style: google.maps.MapTypeControlStyle.DROPDOWN_MENU }
            };

            //Create Groups Map
            var map3 = new google.maps.Map(document.getElementById("MapGroups"), mapSettings);

            ////////////////////////////////////////////////////Polygons for Groups///////////////////////////////////

            var tempGroups = '<%=GetGroupsFromService()%>';

            var groups = tempGroups.split("#");

            var k = 0;

            for (k = 0; k < groups.length; k++) {
                var l = 0;
                var nearbys = groups[k].split("|");
                var nearbyLocs = [];

                for (l = 0; l < nearbys.length; l++) {
                    var locs = nearbys[l].split(";");
                    nearbyLocs.push(new google.maps.LatLng(locs[0], locs[1]));
                }


                var groupPath = new google.maps.Polygon({
                    path: nearbyLocs,
                    strokeColor: "#0000FF",
                    strokeOpacity: 0.8,
                    strokeWeight: 2,
                    fillColor: "#0000FF",
                    fillOpacity: 0.4
                });
                groupPath.setMap(map3);
            }
        }

        function closeNav() {
            document.getElementById("sidenavbar").style.width = "0";
            document.getElementById("main").style.marginLeft = "0";
        }


        function openNav() {
            document.getElementById("sidenavbar").style.width = "250px";
            document.getElementById("main").style.marginLeft = "250px";
        }

    </script>

    <%-- API references --%>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBR0IZ6QgP0p-gLCiremikh65ZaIfHcS3s" async defer></script>
    <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>


    <%-- End of Javascript --%>
</body>
</html>
