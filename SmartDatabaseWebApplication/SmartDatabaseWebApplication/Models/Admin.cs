﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SmartDatabaseWebApplication.Models
{
    public class Admin
    {
        [Required(ErrorMessage = "ID (Mail) is required!")]
        public string id;
        [Required(ErrorMessage = "Password is required!")]
        [DataType(DataType.Password)]
        public string password;
        public string token;

    }
}