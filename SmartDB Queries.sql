﻿
CREATE TABLE admins(id VARCHAR(20) NOT NULL, pass VARCHAR(150) NOT NULL,adminName VARCHAR(30),email VARCHAR(30),salt VARCHAR(150),canRemove int,canAdd int,isActive int,CONSTRAINT admin_pk PRIMARY KEY(id));
CREATE TABLE adminTokens(id VARCHAR(20),tokenString VARCHAR(100),endDate datetime,CONSTRAINT admin_token_pk PRIMARY KEY(id,tokenString),FOREIGN KEY(id) REFERENCES admins(id) ON DELETE CASCADE);
CREATE TABLE disasters(id int IDENTITY(1,1)  primary key, latitude float(10),longitude float(10),radius float, descrp varchar(500), sTime datetime, fTime datetime, isActive int);

------------------------------------------

CREATE PROCEDURE AddToken
	@email VARCHAR(30),
	@tokenString VARCHAR(100),
	@endDate DateTime
AS
BEGIN TRANSACTION 
	INSERT INTO userTokens VALUES(@email,@tokenString,@endDate);
	COMMIT TRANSACTION 
RETURN 0 ;

------------------------------------------

CREATE PROCEDURE AddAdminToken
	@id VARCHAR(20),
	@tokenString VARCHAR(100),
	@endDate DateTime
AS
BEGIN TRANSACTION 
	INSERT INTO adminTokens VALUES(@id,@tokenString,@endDate);
	COMMIT TRANSACTION 
RETURN 0 ;

------------------------------------------

CREATE PROCEDURE CheckAdminToken
	@tokenString VARCHAR(100)
AS
BEGIN 
	SELECT endDate FROM adminTokens WHERE tokenString = @tokenString AND endDate > sysdatetime();
END
RETURN 0;

------------------------------------------

CREATE PROCEDURE CheckToken
	@tokenString VARCHAR(100)
AS
BEGIN 
	SELECT * FROM userTokens WHERE tokenString = @tokenString AND endDate > sysdatetime();
END
RETURN 0;

------------------------------------------

CREATE PROCEDURE GetAdminMail
	@id VARCHAR(20)
AS
BEGIN 
	SELECT email FROM admins WHERE id=@id;
END
RETURN 0;

------------------------------------------

CREATE PROCEDURE AddAdmin
	@id VARCHAR(20),
	@pass VARCHAR(150),
	@name VARCHAR(30),
	@mail VARCHAR(30),
	@salt VARCHAR(150),
	@canRemove int,
	@canAdd int
AS
BEGIN TRANSACTION 
	INSERT INTO admins VALUES(@id,@pass,@name,@mail,@salt,@canRemove,@canAdd,1);
	COMMIT TRANSACTION 
RETURN 0 ;


------------------------------------------
CREATE PROCEDURE GetAdmin
	@id VARCHAR(20)
	
AS
BEGIN
	select email,adminName,canRemove,canAdd from admins where id = @id;
END
RETURN 0 ;

------------------------------------------
CREATE PROCEDURE DeleteAdmin
	@id VARCHAR(20)
	
AS
BEGIN TRANSACTION 
	UPDATE  admins SET isActive = 0 WHERE id = @id;
	COMMIT TRANSACTION 
RETURN 0 ;

------------------------------------------
CREATE PROCEDURE GetAdminList
AS
BEGIN
	select adminName,id from admins where isActive = 1;
END
RETURN 0 ;

------------------------------------------

drop procedure GetAdminList

CREATE PROCEDURE AddUser
	@email VARCHAR(30),
	@pass VARCHAR(20),
	@name VARCHAR(50),
	@bloodType VARCHAR(10)
AS
BEGIN TRANSACTION 
	INSERT INTO userTable VALUES(@name,@email,@pass,0.0,0.0,'',NULL,@bloodType);
	COMMIT TRANSACTION 
RETURN 0 ;

-----------------------------------------
CREATE PROCEDURE AddDisaster
	@latitude float(10),
	@longitude float(10),
	@radius float(10),
	@descrp varchar(500)
	
AS
BEGIN TRANSACTION 

	INSERT INTO disasters(latitude,longitude,radius,descrp,sTime,fTime,isActive) VALUES(@latitude,@longitude,@radius,@descrp,sysdatetime(),sysdatetime(),1);
	COMMIT TRANSACTION 
RETURN 0 ;


------------------------------------------
CREATE PROCEDURE DeleteDisaster
	@id int
AS
BEGIN TRANSACTION 
	UPDATE disasters SET isActive = 0,fTime = sysdatetime() WHERE id = @id;
	COMMIT TRANSACTION ;
RETURN 0;


------------------------------------------
CREATE PROCEDURE GetDisasters
AS
BEGIN
	select * from disasters;
END
RETURN 0 ;

------------------------------------------

CREATE PROCEDURE GetActiveDisasters
AS
BEGIN
	select * from disasters where isActive = 1;
END
RETURN 0 ;

------------------------------------------



